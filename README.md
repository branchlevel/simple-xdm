# Simple XDM

A simple library that allows to expose host API to an embedded iframe through the `postMessage` protocol.

__Work in progress...__

## Goals

* No changes necessary in the iframe library when the host API changes.
* Tiny (< 2KB, both for the host and the iframe library).
* No DOM modifications (plays well with [React.js](https://facebook.github.io/react/)).
* No dependencies, vanilla JavaScript.
* Works with different module loaders using [UMD](https://github.com/umdjs/umd/blob/master/returnExports.js):
 AMD, Common.js and globals.
* Supports modern browsers (ES5+, i.e. IE9 and upwards, not tested yet beyond Chrome and Firefox)

## Usage

### Build

    npm install
    gulp
    
This command generates the two minified libs for the host and the iframe:

* dist/host.min.js
* dist/iframe.min.js

### Define your API

Include `/src/host.js` or `simple-xdm-host.min.js` on the host side. See `/product/product.html` for an example.
Then you can use `AP.defineModule` to define your API (see `/product/product.js`):

    AP.defineModule('messages', {
        error: function(title, body, options) {
            // your code
        },
        info: function(title, body, options) {
            // your code
        },
        success: function(title, body, options) {
            // your code
        },
        warning: function(title, body, options) {
            // your code
        },
    });
    
Code in the iframe can call this API using:

    AP.require('messages', function(messages) {
        messages.error('Error', message)
    });

You can also define globals:

    AP.defineGlobals({
       request: function(options, cb) {
           setTimeout(function() {
               cb({statusCode: 200, response: options});
           }, 10);
       }
    });
    
Code in the iframe can call this API using:

    AP.request({ ... }, function(result) {
        // do something
    });
    
### Use the API in the iframe

Include `/src/iframe.js` or `simple-xdm-iframe.min.js` on the iframe side. See `/addon/add-on.html` for an example.

Use the API either through globals or modules:

    AP.require('messages', function(messages) {
        messages.error('Error', message)
    });
    
    AP.request({ ... }, function(result) {
        // do something
    });
    
### Create an iframe

The library does not modify the DOM directly, iframe creation is delegated to the host product. 
Here's how you create an iframe that will be able to call the host API through the `simple-xdm` bridge: 

    function setup(extension) {

        var iframeParams = AP.create(extension, init);

        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', iframeParams.id);
        iframe.setAttribute('name', iframeParams.name);
        iframe.setAttribute('src', iframeParams.src);
        iframe.setAttribute('frameBorder', 0);
        document.getElementById(extension.moduleKey).appendChild(iframe);
        return true;
    }

    function init(extensionId) {
        console.log('Bridge established:' + extensionId);
    }

    setup({
        addonKey: 'my-addon',
        moduleKey: 'my-panel',
        url: 'http://localhost:8080/addon/add-on.html'
    });

### Events

iframes can also handle events dispatched by the host. A callback can optionally be used to return a result.

Event registration in the iframe:

    AP.register({
        'some-event': function (event, cb) {
            cb('Some response');
        },
        'some-other-event': function(event) {
            // do something
        }
    });
    
The host side can send events to a specific add-on using

    AP.dispatch('some-event',
        {addonKey: extension.addonKey},
        {message: message},
        function (result) {
            // do something
        }
    );
    
Or it can target a specific module:

    AP.dispatch('some-event',
        {addonKey: extension.addonKey, moduleKey: extension.moduleKey},
        {message: message},
        function (result) {
            // do something
        }
    );
    
Events can also be broadcast to all add-ons on the page:

    AP.dispatch('some-other-event', {}, {message: message});
    
    
### Conventions

* Callbacks must be the last argument in the function declaration
* Callbacks must be the only function in the argument list
* Callbacks are optional
* All function parameters besides the callback must be serializable through the [Structured clone algorithm](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/The_structured_clone_algorithm)

## Try it

In the project directory:

```
http-server
```

Then point your browser to [http://localhost:8080/product/product.html](http://localhost:8080/product/product.html)

To test the actual cross-domain case, you can serve the local project through ngrok:

```
ngrok -log=stdout 8080
```

Then point your browser to your ngrok URL: [http://<your-id>.ngrok.com/product/product.html](http://42782db2.ngrok.com/product/product.html)

### Prerequisites

Install the HTTP server:

    npm install -g http-server

[Install ngrok](https://ngrok.com/download)