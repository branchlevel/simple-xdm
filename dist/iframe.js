(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.AP = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _commonUtil = _dereq_('../common/util');

var _commonUtil2 = _interopRequireDefault(_commonUtil);

var _commonPostmessage = _dereq_('../common/postmessage');

var _commonPostmessage2 = _interopRequireDefault(_commonPostmessage);

var AP = (function (_PostMessage) {
  _inherits(AP, _PostMessage);

  function AP() {
    _classCallCheck(this, AP);

    _get(Object.getPrototypeOf(AP.prototype), 'constructor', this).call(this);
    this._data = this._parseInitData();
    this._host = window.parent;
    this._hostModules = {};
    this._eventHandlers = {};
    this._pendingCallbacks = {};
    this._setupAPI(this._data.api);
    this._messageHandlers = {
      resp: this._handleResponse,
      evt: this._handleEvent
    };

    this._sendInit();
  }

  /**
  * The initialization data is passed in when the iframe is created as its 'name' attribute.
  * Example:
  * {
  *   extension_id: The ID of this iframe as defined by the host
  *   origin: 'https://example.org'  // The parent's window origin
  *   api: {
  *     _globals: { ... },
  *     messages = {
  *       clear: {},
  *       ...
  *     },
  *     ...
  *   }
  * }
  **/

  _createClass(AP, [{
    key: '_parseInitData',
    value: function _parseInitData(data) {
      try {
        return JSON.parse(data || window.name);
      } catch (e) {
        return {};
      }
    }
  }, {
    key: '_createModule',
    value: function _createModule(moduleName, api) {
      var _this = this;

      return Object.getOwnPropertyNames(api).reduce(function (accumulator, functionName) {
        accumulator[functionName] = _this._createMethodHandler({
          mod: moduleName,
          fn: functionName
        });
        return accumulator;
      }, {});
    }
  }, {
    key: '_setupAPI',
    value: function _setupAPI(api) {
      var _this2 = this;

      this._hostModules = Object.getOwnPropertyNames(api).reduce(function (accumulator, moduleName) {
        accumulator[moduleName] = _this2._createModule(moduleName, api[moduleName]);
        return accumulator;
      }, {});

      Object.getOwnPropertyNames(this._hostModules._globals || {}).forEach(function (global) {
        _this2[global] = _this2._hostModules._globals[global];
      });
    }
  }, {
    key: '_pendingCallback',
    value: function _pendingCallback(mid, fn) {
      this._pendingCallbacks[mid] = fn;
    }
  }, {
    key: '_createMethodHandler',
    value: function _createMethodHandler(methodData) {
      var methodHandler = function methodHandler() {
        var mid = undefined,
            args = _commonUtil2['default'].argumentsToArray(arguments);
        if (_commonUtil2['default'].hasCallback(args)) {
          mid = _commonUtil2['default'].randomString();
          this._pendingCallback(mid, args.pop());
        }
        this._host.postMessage({
          eid: this._data.extension_id,
          type: 'req',
          mid: mid,
          mod: methodData.mod,
          fn: methodData.fn,
          args: args
        }, this._data.origin);
      };

      return _commonUtil2['default']._bind(this, methodHandler);
    }
  }, {
    key: '_handleResponse',
    value: function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];
      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        pendingCallback.apply(window, data.args);
      }
    }
  }, {
    key: '_handleEvent',
    value: function _handleEvent(event) {
      var sendResponse = function sendResponse() {
        var args = _commonUtil2['default'].argumentsToArray(arguments);
        event.source.postMessage({
          eid: this._data.extension_id,
          mid: event.data.mid,
          type: 'resp',
          args: args
        }, this._data.origin);
      };
      sendResponse = _commonUtil2['default']._bind(this, sendResponse);
      var data = event.data;
      var handler = this._eventHandlers[data.etyp];
      if (handler) {
        handler(data.evnt, sendResponse);
      } else if (data.mid) {
        sendResponse();
      }
    }
  }, {
    key: '_checkOrigin',
    value: function _checkOrigin(event) {
      return event.origin === this._data.origin && event.source === this._host;
    }
  }, {
    key: '_sendInit',
    value: function _sendInit() {
      this._host.postMessage({
        eid: this._data.extension_id,
        type: 'init'
      }, this._data.origin);
    }
  }, {
    key: 'require',
    value: function _dereq_(modules, callback) {
      var _this3 = this;

      var requiredModules = Array.isArray(modules) ? modules : [modules],
          args = requiredModules.map(function (module) {
        return _this3._hostModules[module];
      });
      callback.apply(window, args);
    }
  }, {
    key: 'register',
    value: function register(handlers) {
      this._eventHandlers = handlers || {};
      this._host.postMessage({
        eid: this._data.extension_id,
        type: 'event_query',
        args: Object.getOwnPropertyNames(handlers)
      }, this._data.origin);
    }
  }]);

  return AP;
})(_commonPostmessage2['default']);

module.exports = new AP();

},{"../common/postmessage":2,"../common/util":3}],2:[function(_dereq_,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _util = _dereq_('./util');

var _util2 = _interopRequireDefault(_util);

var PostMessage = (function () {
  function PostMessage(data) {
    _classCallCheck(this, PostMessage);

    var d = data || {};
    this._registerListener(d.listenOn);
  }

  // listen for postMessage events (defaults to window).

  _createClass(PostMessage, [{
    key: "_registerListener",
    value: function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }
      listenOn.addEventListener("message", _util2["default"]._bind(this, this._receiveMessage), false);
    }
  }, {
    key: "_receiveMessage",
    value: function _receiveMessage(event) {
      var extensionId = event.data.eid,
          reg = undefined;

      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }

      if (!this._checkOrigin(event, reg)) {
        return false;
      }

      var handler = this._messageHandlers[event.data.type];
      if (handler) {
        handler.call(this, event, reg);
      }
    }
  }]);

  return PostMessage;
})();

module.exports = PostMessage;

},{"./util":3}],3:[function(_dereq_,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LOG_PREFIX = "[Simple-XDM] ";

var Util = (function () {
  function Util() {
    _classCallCheck(this, Util);
  }

  _createClass(Util, [{
    key: "randomString",
    value: function randomString() {
      return Math.floor(Math.random() * 1000000000).toString(16);
    }

    // might be un-needed
  }, {
    key: "argumentsToArray",
    value: function argumentsToArray(arrayLike) {
      var array = [];
      for (var i = 0; i < arrayLike.length; i++) {
        array.push(arrayLike[i]);
      }
      return array;
    }
  }, {
    key: "hasCallback",
    value: function hasCallback(args) {
      var length = args.length;
      return length > 0 && typeof args[length - 1] === 'function';
    }
  }, {
    key: "error",
    value: function error(msg) {
      console.error(LOG_PREFIX + msg);
    }
  }, {
    key: "warn",
    value: function warn(msg) {
      console.warn(LOG_PREFIX + msg);
    }
  }, {
    key: "_bind",
    value: function _bind(thisp, fn) {
      if (Function.prototype.bind) {
        return fn.bind(thisp);
      }
      return function () {
        return fn.apply(thisp, arguments);
      };
    }
  }]);

  return Util;
})();

module.exports = new Util();

},{}]},{},[1])(1)
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvc3R1ZmYvaGlwY2hhdC9zaW1wbGUteGRtL3NyYy9wbHVnaW4vaW5kZXguanMiLCIvc3R1ZmYvaGlwY2hhdC9zaW1wbGUteGRtL3NyYy9jb21tb24vcG9zdG1lc3NhZ2UuanMiLCIvc3R1ZmYvaGlwY2hhdC9zaW1wbGUteGRtL3NyYy9jb21tb24vdXRpbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7OzswQkNBaUIsZ0JBQWdCOzs7O2lDQUNULHVCQUF1Qjs7OztJQUV6QyxFQUFFO1lBQUYsRUFBRTs7QUFFSyxXQUZQLEVBQUUsR0FFUTswQkFGVixFQUFFOztBQUdKLCtCQUhFLEVBQUUsNkNBR0k7QUFDUixRQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztBQUNuQyxRQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7QUFDM0IsUUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7QUFDdkIsUUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7QUFDekIsUUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztBQUM1QixRQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDL0IsUUFBSSxDQUFDLGdCQUFnQixHQUFHO0FBQ3BCLFVBQUksRUFBRSxJQUFJLENBQUMsZUFBZTtBQUMxQixTQUFHLEVBQUUsSUFBSSxDQUFDLFlBQVk7S0FDekIsQ0FBQzs7QUFFRixRQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7R0FDbEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUFoQkcsRUFBRTs7V0FpQ1Esd0JBQUMsSUFBSSxFQUFFO0FBQ25CLFVBQUk7QUFDRixlQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztPQUN4QyxDQUFDLE9BQU8sQ0FBQyxFQUFFO0FBQ1YsZUFBTyxFQUFFLENBQUM7T0FDWDtLQUNGOzs7V0FFWSx1QkFBQyxVQUFVLEVBQUUsR0FBRyxFQUFFOzs7QUFDN0IsYUFBTyxNQUFNLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsV0FBVyxFQUFFLFlBQVksRUFBSztBQUMzRSxtQkFBVyxDQUFDLFlBQVksQ0FBQyxHQUFHLE1BQUssb0JBQW9CLENBQUM7QUFDbEQsYUFBRyxFQUFFLFVBQVU7QUFDZixZQUFFLEVBQUUsWUFBWTtTQUNuQixDQUFDLENBQUM7QUFDSCxlQUFPLFdBQVcsQ0FBQztPQUNwQixFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQ1I7OztXQUVRLG1CQUFDLEdBQUcsRUFBRTs7O0FBQ2IsVUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsV0FBVyxFQUFFLFVBQVUsRUFBSztBQUNwRixtQkFBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQUssYUFBYSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztBQUMxRSxlQUFPLFdBQVcsQ0FBQztPQUN0QixFQUFFLEVBQUUsQ0FBQyxDQUFDOztBQUVQLFlBQU0sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDN0UsZUFBSyxNQUFNLENBQUMsR0FBRyxPQUFLLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7T0FDckQsQ0FBQyxDQUFDO0tBQ0o7OztXQUVlLDBCQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUM7QUFDdkIsVUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztLQUNsQzs7O1dBRW1CLDhCQUFDLFVBQVUsRUFBRTtBQUMvQixVQUFJLGFBQWEsR0FBRyxTQUFoQixhQUFhLEdBQWU7QUFDOUIsWUFBSSxHQUFHLFlBQUE7WUFDSCxJQUFJLEdBQUcsd0JBQUssZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDNUMsWUFBSSx3QkFBSyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUU7QUFDMUIsYUFBRyxHQUFHLHdCQUFLLFlBQVksRUFBRSxDQUFDO0FBQzFCLGNBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7U0FDeEM7QUFDRCxZQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztBQUNuQixhQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO0FBQzVCLGNBQUksRUFBRSxLQUFLO0FBQ1gsYUFBRyxFQUFFLEdBQUc7QUFDUixhQUFHLEVBQUUsVUFBVSxDQUFDLEdBQUc7QUFDbkIsWUFBRSxFQUFFLFVBQVUsQ0FBQyxFQUFFO0FBQ2pCLGNBQUksRUFBRSxJQUFJO1NBQ2IsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO09BQ3ZCLENBQUM7O0FBRUYsYUFBTyx3QkFBSyxLQUFLLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0tBQ3hDOzs7V0FFYyx5QkFBQyxLQUFLLEVBQUU7QUFDckIsVUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztBQUN0QixVQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3ZELFVBQUksZUFBZSxFQUFFO0FBQ25CLGVBQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUN4Qyx1QkFBZSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO09BQzFDO0tBQ0Y7OztXQUVXLHNCQUFDLEtBQUssRUFBRTtBQUNsQixVQUFJLFlBQVksR0FBRyx3QkFBWTtBQUM3QixZQUFJLElBQUksR0FBRyx3QkFBSyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUM1QyxhQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztBQUNyQixhQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZO0FBQzVCLGFBQUcsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUc7QUFDbkIsY0FBSSxFQUFFLE1BQU07QUFDWixjQUFJLEVBQUUsSUFBSTtTQUNiLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztPQUN2QixDQUFDO0FBQ0Ysa0JBQVksR0FBRyx3QkFBSyxLQUFLLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDO0FBQzlDLFVBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7QUFDdEIsVUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDN0MsVUFBSSxPQUFPLEVBQUU7QUFDVCxlQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztPQUNwQyxNQUFNLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtBQUNqQixvQkFBWSxFQUFFLENBQUM7T0FDbEI7S0FDRjs7O1dBRVcsc0JBQUMsS0FBSyxFQUFFO0FBQ2hCLGFBQU8sS0FBSyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDNUU7OztXQUVRLHFCQUFHO0FBQ1YsVUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7QUFDbkIsV0FBRyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWTtBQUM1QixZQUFJLEVBQUUsTUFBTTtPQUNmLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUN2Qjs7O1dBRU0saUJBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRTs7O0FBQ3pCLFVBQUksZUFBZSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDO1VBQzlELElBQUksR0FBRyxlQUFlLENBQUMsR0FBRyxDQUFDLFVBQUMsTUFBTSxFQUFLO0FBQ3JDLGVBQU8sT0FBSyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7T0FDbEMsQ0FBQyxDQUFDO0FBQ1AsY0FBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDOUI7OztXQUVPLGtCQUFDLFFBQVEsRUFBRTtBQUNqQixVQUFJLENBQUMsY0FBYyxHQUFHLFFBQVEsSUFBSSxFQUFFLENBQUM7QUFDckMsVUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7QUFDckIsV0FBRyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWTtBQUM1QixZQUFJLEVBQUUsYUFBYTtBQUNuQixZQUFJLEVBQUUsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQztPQUMzQyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDdkI7OztTQTlJRyxFQUFFOzs7QUFrSlIsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLEVBQUUsRUFBRSxDQUFDOzs7Ozs7Ozs7OztvQkNySlQsUUFBUTs7OztJQUNuQixXQUFXO0FBRUosV0FGUCxXQUFXLENBRUgsSUFBSSxFQUFFOzBCQUZkLFdBQVc7O0FBR2IsUUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztBQUNuQixRQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0dBQ3BDOzs7O2VBTEcsV0FBVzs7V0FRRSwyQkFBQyxRQUFRLEVBQUU7QUFDMUIsVUFBRyxDQUFDLFFBQVEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRTtBQUMxQyxnQkFBUSxHQUFHLE1BQU0sQ0FBQztPQUNuQjtBQUNELGNBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsa0JBQUssS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDckY7OztXQUVlLHlCQUFDLEtBQUssRUFBRTtBQUN0QixVQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUc7VUFDaEMsR0FBRyxZQUFBLENBQUM7O0FBRUosVUFBRyxXQUFXLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFDO0FBQzNDLFdBQUcsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLENBQUM7T0FDL0M7O0FBRUQsVUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxFQUFFO0FBQ2xDLGVBQU8sS0FBSyxDQUFDO09BQ2Q7O0FBRUQsVUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDckQsVUFBSSxPQUFPLEVBQUU7QUFDWCxlQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7T0FDaEM7S0FDRjs7O1NBL0JHLFdBQVc7OztBQW1DakIsTUFBTSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUM7Ozs7Ozs7OztBQ3BDN0IsSUFBTSxVQUFVLEdBQUcsZUFBZSxDQUFDOztJQUU3QixJQUFJO1dBQUosSUFBSTswQkFBSixJQUFJOzs7ZUFBSixJQUFJOztXQUNJLHdCQUFHO0FBQ2IsYUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDNUQ7Ozs7O1dBRWUsMEJBQUMsU0FBUyxFQUFFO0FBQzFCLFVBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztBQUNmLFdBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ3pDLGFBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7T0FDMUI7QUFDRCxhQUFPLEtBQUssQ0FBQztLQUNkOzs7V0FFVSxxQkFBQyxJQUFJLEVBQUU7QUFDaEIsVUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztBQUN6QixhQUFPLE1BQU0sR0FBRyxDQUFDLElBQUksT0FBTyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLFVBQVUsQ0FBQztLQUM3RDs7O1dBRUksZUFBQyxHQUFHLEVBQUU7QUFDUCxhQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQztLQUNuQzs7O1dBRUcsY0FBQyxHQUFHLEVBQUU7QUFDTixhQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQztLQUNsQzs7O1dBRUksZUFBQyxLQUFLLEVBQUUsRUFBRSxFQUFDO0FBQ2QsVUFBRyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRTtBQUMxQixlQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7T0FDdkI7QUFDRCxhQUFPLFlBQVk7QUFDakIsZUFBTyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQztPQUNuQyxDQUFDO0tBQ0g7OztTQWpDRyxJQUFJOzs7QUF1Q1YsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImltcG9ydCBVdGlsIGZyb20gJy4uL2NvbW1vbi91dGlsJztcbmltcG9ydCBQb3N0TWVzc2FnZSBmcm9tICcuLi9jb21tb24vcG9zdG1lc3NhZ2UnO1xuXG5jbGFzcyBBUCBleHRlbmRzIFBvc3RNZXNzYWdlIHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMuX2RhdGEgPSB0aGlzLl9wYXJzZUluaXREYXRhKCk7XG4gICAgdGhpcy5faG9zdCA9IHdpbmRvdy5wYXJlbnQ7XG4gICAgdGhpcy5faG9zdE1vZHVsZXMgPSB7fTtcbiAgICB0aGlzLl9ldmVudEhhbmRsZXJzID0ge307XG4gICAgdGhpcy5fcGVuZGluZ0NhbGxiYWNrcyA9IHt9O1xuICAgIHRoaXMuX3NldHVwQVBJKHRoaXMuX2RhdGEuYXBpKTtcbiAgICB0aGlzLl9tZXNzYWdlSGFuZGxlcnMgPSB7XG4gICAgICAgIHJlc3A6IHRoaXMuX2hhbmRsZVJlc3BvbnNlLFxuICAgICAgICBldnQ6IHRoaXMuX2hhbmRsZUV2ZW50XG4gICAgfTtcblxuICAgIHRoaXMuX3NlbmRJbml0KCk7XG4gIH1cbiAgLyoqXG4gICogVGhlIGluaXRpYWxpemF0aW9uIGRhdGEgaXMgcGFzc2VkIGluIHdoZW4gdGhlIGlmcmFtZSBpcyBjcmVhdGVkIGFzIGl0cyAnbmFtZScgYXR0cmlidXRlLlxuICAqIEV4YW1wbGU6XG4gICoge1xuICAqICAgZXh0ZW5zaW9uX2lkOiBUaGUgSUQgb2YgdGhpcyBpZnJhbWUgYXMgZGVmaW5lZCBieSB0aGUgaG9zdFxuICAqICAgb3JpZ2luOiAnaHR0cHM6Ly9leGFtcGxlLm9yZycgIC8vIFRoZSBwYXJlbnQncyB3aW5kb3cgb3JpZ2luXG4gICogICBhcGk6IHtcbiAgKiAgICAgX2dsb2JhbHM6IHsgLi4uIH0sXG4gICogICAgIG1lc3NhZ2VzID0ge1xuICAqICAgICAgIGNsZWFyOiB7fSxcbiAgKiAgICAgICAuLi5cbiAgKiAgICAgfSxcbiAgKiAgICAgLi4uXG4gICogICB9XG4gICogfVxuICAqKi9cbiAgX3BhcnNlSW5pdERhdGEoZGF0YSkge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gSlNPTi5wYXJzZShkYXRhIHx8IHdpbmRvdy5uYW1lKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4ge307XG4gICAgfVxuICB9XG5cbiAgX2NyZWF0ZU1vZHVsZShtb2R1bGVOYW1lLCBhcGkpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoYXBpKS5yZWR1Y2UoKGFjY3VtdWxhdG9yLCBmdW5jdGlvbk5hbWUpID0+IHtcbiAgICAgIGFjY3VtdWxhdG9yW2Z1bmN0aW9uTmFtZV0gPSB0aGlzLl9jcmVhdGVNZXRob2RIYW5kbGVyKHtcbiAgICAgICAgICBtb2Q6IG1vZHVsZU5hbWUsXG4gICAgICAgICAgZm46IGZ1bmN0aW9uTmFtZVxuICAgICAgfSk7XG4gICAgICByZXR1cm4gYWNjdW11bGF0b3I7XG4gICAgfSwge30pO1xuICB9XG5cbiAgX3NldHVwQVBJKGFwaSkge1xuICAgIHRoaXMuX2hvc3RNb2R1bGVzID0gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoYXBpKS5yZWR1Y2UoKGFjY3VtdWxhdG9yLCBtb2R1bGVOYW1lKSA9PiB7XG4gICAgICAgIGFjY3VtdWxhdG9yW21vZHVsZU5hbWVdID0gdGhpcy5fY3JlYXRlTW9kdWxlKG1vZHVsZU5hbWUsIGFwaVttb2R1bGVOYW1lXSk7XG4gICAgICAgIHJldHVybiBhY2N1bXVsYXRvcjtcbiAgICB9LCB7fSk7XG5cbiAgICBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0aGlzLl9ob3N0TW9kdWxlcy5fZ2xvYmFscyB8fCB7fSkuZm9yRWFjaCgoZ2xvYmFsKSA9PiB7XG4gICAgICAgIHRoaXNbZ2xvYmFsXSA9IHRoaXMuX2hvc3RNb2R1bGVzLl9nbG9iYWxzW2dsb2JhbF07XG4gICAgfSk7XG4gIH1cblxuICBfcGVuZGluZ0NhbGxiYWNrKG1pZCwgZm4pe1xuICAgIHRoaXMuX3BlbmRpbmdDYWxsYmFja3NbbWlkXSA9IGZuO1xuICB9XG5cbiAgX2NyZWF0ZU1ldGhvZEhhbmRsZXIobWV0aG9kRGF0YSkge1xuICAgIGxldCBtZXRob2RIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgbGV0IG1pZCxcbiAgICAgICAgICBhcmdzID0gVXRpbC5hcmd1bWVudHNUb0FycmF5KGFyZ3VtZW50cyk7XG4gICAgICBpZiAoVXRpbC5oYXNDYWxsYmFjayhhcmdzKSkge1xuICAgICAgICBtaWQgPSBVdGlsLnJhbmRvbVN0cmluZygpO1xuICAgICAgICB0aGlzLl9wZW5kaW5nQ2FsbGJhY2sobWlkLCBhcmdzLnBvcCgpKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuX2hvc3QucG9zdE1lc3NhZ2Uoe1xuICAgICAgICAgIGVpZDogdGhpcy5fZGF0YS5leHRlbnNpb25faWQsXG4gICAgICAgICAgdHlwZTogJ3JlcScsXG4gICAgICAgICAgbWlkOiBtaWQsXG4gICAgICAgICAgbW9kOiBtZXRob2REYXRhLm1vZCxcbiAgICAgICAgICBmbjogbWV0aG9kRGF0YS5mbixcbiAgICAgICAgICBhcmdzOiBhcmdzXG4gICAgICB9LCB0aGlzLl9kYXRhLm9yaWdpbik7XG4gICAgfTtcblxuICAgIHJldHVybiBVdGlsLl9iaW5kKHRoaXMsIG1ldGhvZEhhbmRsZXIpO1xuICB9XG5cbiAgX2hhbmRsZVJlc3BvbnNlKGV2ZW50KSB7XG4gICAgdmFyIGRhdGEgPSBldmVudC5kYXRhO1xuICAgIHZhciBwZW5kaW5nQ2FsbGJhY2sgPSB0aGlzLl9wZW5kaW5nQ2FsbGJhY2tzW2RhdGEubWlkXTtcbiAgICBpZiAocGVuZGluZ0NhbGxiYWNrKSB7XG4gICAgICBkZWxldGUgdGhpcy5fcGVuZGluZ0NhbGxiYWNrc1tkYXRhLm1pZF07XG4gICAgICBwZW5kaW5nQ2FsbGJhY2suYXBwbHkod2luZG93LCBkYXRhLmFyZ3MpO1xuICAgIH1cbiAgfVxuXG4gIF9oYW5kbGVFdmVudChldmVudCkge1xuICAgIHZhciBzZW5kUmVzcG9uc2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgYXJncyA9IFV0aWwuYXJndW1lbnRzVG9BcnJheShhcmd1bWVudHMpO1xuICAgICAgZXZlbnQuc291cmNlLnBvc3RNZXNzYWdlKHtcbiAgICAgICAgICBlaWQ6IHRoaXMuX2RhdGEuZXh0ZW5zaW9uX2lkLFxuICAgICAgICAgIG1pZDogZXZlbnQuZGF0YS5taWQsXG4gICAgICAgICAgdHlwZTogJ3Jlc3AnLFxuICAgICAgICAgIGFyZ3M6IGFyZ3NcbiAgICAgIH0sIHRoaXMuX2RhdGEub3JpZ2luKTtcbiAgICB9O1xuICAgIHNlbmRSZXNwb25zZSA9IFV0aWwuX2JpbmQodGhpcywgc2VuZFJlc3BvbnNlKTtcbiAgICB2YXIgZGF0YSA9IGV2ZW50LmRhdGE7XG4gICAgdmFyIGhhbmRsZXIgPSB0aGlzLl9ldmVudEhhbmRsZXJzW2RhdGEuZXR5cF07XG4gICAgaWYgKGhhbmRsZXIpIHtcbiAgICAgICAgaGFuZGxlcihkYXRhLmV2bnQsIHNlbmRSZXNwb25zZSk7XG4gICAgfSBlbHNlIGlmIChkYXRhLm1pZCkge1xuICAgICAgICBzZW5kUmVzcG9uc2UoKTtcbiAgICB9XG4gIH1cblxuICBfY2hlY2tPcmlnaW4oZXZlbnQpIHtcbiAgICAgIHJldHVybiBldmVudC5vcmlnaW4gPT09IHRoaXMuX2RhdGEub3JpZ2luICYmIGV2ZW50LnNvdXJjZSA9PT0gdGhpcy5faG9zdDtcbiAgfVxuXG4gIF9zZW5kSW5pdCgpIHtcbiAgICB0aGlzLl9ob3N0LnBvc3RNZXNzYWdlKHtcbiAgICAgICAgZWlkOiB0aGlzLl9kYXRhLmV4dGVuc2lvbl9pZCxcbiAgICAgICAgdHlwZTogJ2luaXQnXG4gICAgfSwgdGhpcy5fZGF0YS5vcmlnaW4pO1xuICB9XG5cbiAgcmVxdWlyZShtb2R1bGVzLCBjYWxsYmFjaykge1xuICAgIGxldCByZXF1aXJlZE1vZHVsZXMgPSBBcnJheS5pc0FycmF5KG1vZHVsZXMpID8gbW9kdWxlcyA6IFttb2R1bGVzXSxcbiAgICAgICAgYXJncyA9IHJlcXVpcmVkTW9kdWxlcy5tYXAoKG1vZHVsZSkgPT4ge1xuICAgICAgICAgIHJldHVybiB0aGlzLl9ob3N0TW9kdWxlc1ttb2R1bGVdO1xuICAgICAgICB9KTtcbiAgICBjYWxsYmFjay5hcHBseSh3aW5kb3csIGFyZ3MpO1xuICB9XG5cbiAgcmVnaXN0ZXIoaGFuZGxlcnMpIHtcbiAgICB0aGlzLl9ldmVudEhhbmRsZXJzID0gaGFuZGxlcnMgfHwge307XG4gICAgdGhpcy5faG9zdC5wb3N0TWVzc2FnZSh7XG4gICAgICBlaWQ6IHRoaXMuX2RhdGEuZXh0ZW5zaW9uX2lkLFxuICAgICAgdHlwZTogJ2V2ZW50X3F1ZXJ5JyxcbiAgICAgIGFyZ3M6IE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKGhhbmRsZXJzKVxuICAgIH0sIHRoaXMuX2RhdGEub3JpZ2luKTtcbiAgfVxuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gbmV3IEFQKCk7XG4iLCJpbXBvcnQgVXRpbCBmcm9tICcuL3V0aWwnO1xuY2xhc3MgUG9zdE1lc3NhZ2Uge1xuXG4gIGNvbnN0cnVjdG9yKGRhdGEpIHtcbiAgICBsZXQgZCA9IGRhdGEgfHwge307XG4gICAgdGhpcy5fcmVnaXN0ZXJMaXN0ZW5lcihkLmxpc3Rlbk9uKTtcbiAgfVxuXG4gLy8gbGlzdGVuIGZvciBwb3N0TWVzc2FnZSBldmVudHMgKGRlZmF1bHRzIHRvIHdpbmRvdykuXG4gIF9yZWdpc3Rlckxpc3RlbmVyKGxpc3Rlbk9uKSB7XG4gICAgaWYoIWxpc3Rlbk9uIHx8ICFsaXN0ZW5Pbi5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICBsaXN0ZW5PbiA9IHdpbmRvdztcbiAgICB9XG4gICAgbGlzdGVuT24uYWRkRXZlbnRMaXN0ZW5lcihcIm1lc3NhZ2VcIiwgVXRpbC5fYmluZCh0aGlzLCB0aGlzLl9yZWNlaXZlTWVzc2FnZSksIGZhbHNlKTtcbiAgfVxuXG4gIF9yZWNlaXZlTWVzc2FnZSAoZXZlbnQpIHtcbiAgICBsZXQgZXh0ZW5zaW9uSWQgPSBldmVudC5kYXRhLmVpZCxcbiAgICByZWc7XG5cbiAgICBpZihleHRlbnNpb25JZCAmJiB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9ucyl7XG4gICAgICByZWcgPSB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9uc1tleHRlbnNpb25JZF07XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLl9jaGVja09yaWdpbihldmVudCwgcmVnKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIHZhciBoYW5kbGVyID0gdGhpcy5fbWVzc2FnZUhhbmRsZXJzW2V2ZW50LmRhdGEudHlwZV07XG4gICAgaWYgKGhhbmRsZXIpIHtcbiAgICAgIGhhbmRsZXIuY2FsbCh0aGlzLCBldmVudCwgcmVnKTtcbiAgICB9XG4gIH1cblxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFBvc3RNZXNzYWdlOyIsImNvbnN0IExPR19QUkVGSVggPSBcIltTaW1wbGUtWERNXSBcIjtcblxuY2xhc3MgVXRpbCB7XG4gIHJhbmRvbVN0cmluZygpIHtcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogMTAwMDAwMDAwMCkudG9TdHJpbmcoMTYpO1xuICB9XG4gIC8vIG1pZ2h0IGJlIHVuLW5lZWRlZFxuICBhcmd1bWVudHNUb0FycmF5KGFycmF5TGlrZSkge1xuICAgIHZhciBhcnJheSA9IFtdO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJyYXlMaWtlLmxlbmd0aDsgaSsrKSB7XG4gICAgICBhcnJheS5wdXNoKGFycmF5TGlrZVtpXSk7XG4gICAgfVxuICAgIHJldHVybiBhcnJheTtcbiAgfVxuXG4gIGhhc0NhbGxiYWNrKGFyZ3MpIHtcbiAgICB2YXIgbGVuZ3RoID0gYXJncy5sZW5ndGg7XG4gICAgcmV0dXJuIGxlbmd0aCA+IDAgJiYgdHlwZW9mIGFyZ3NbbGVuZ3RoIC0gMV0gPT09ICdmdW5jdGlvbic7XG4gIH1cblxuICBlcnJvcihtc2cpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoTE9HX1BSRUZJWCArIG1zZyk7XG4gIH1cblxuICB3YXJuKG1zZykge1xuICAgICAgY29uc29sZS53YXJuKExPR19QUkVGSVggKyBtc2cpO1xuICB9XG5cbiAgX2JpbmQodGhpc3AsIGZuKXtcbiAgICBpZihGdW5jdGlvbi5wcm90b3R5cGUuYmluZCkge1xuICAgICAgcmV0dXJuIGZuLmJpbmQodGhpc3ApO1xuICAgIH1cbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIGZuLmFwcGx5KHRoaXNwLCBhcmd1bWVudHMpO1xuICAgIH07XG4gIH1cblxuXG5cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBuZXcgVXRpbCgpOyJdfQ==
