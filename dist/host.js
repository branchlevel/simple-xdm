(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.host = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _xdmrpc = _dereq_('./xdmrpc');

var _xdmrpc2 = _interopRequireDefault(_xdmrpc);

var _commonUtil = _dereq_('../common/util');

var _commonUtil2 = _interopRequireDefault(_commonUtil);

var Connect = (function () {
  function Connect() {
    _classCallCheck(this, Connect);

    this._xdm = new _xdmrpc2['default']();
  }

  _createClass(Connect, [{
    key: 'dispatch',
    value: function dispatch(type, targetSpec, event, callback) {
      this._xdm.queueEvent(type, targetSpec, event, callback);
      return this.getExtensions(targetSpec);
    }
  }, {
    key: '_createId',
    value: function _createId(extension) {
      if (!extension.addon_key || !extension.key) {
        throw Error('Extensions require addon_key and key');
      }
      return extension.addon_key + '__' + extension.key + '__' + _commonUtil2['default'].randomString();
    }

    /**
    * Creates a new iframed module, without actually creating the DOM element.
    * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
    * the DOM element and returning the window reference.
    *
    * @param extension The extension definition. Example:
    *   {
    *     addonKey: 'my-addon',
    *     moduleKey: 'my-module',
    *     url: 'https://example.com/my-module'
    *   }
    *
    * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
    **/
  }, {
    key: 'create',
    value: function create(extension, initCallback) {
      var extension_id = this.registerExtension(extension, initCallback);

      var data = {
        extension_id: extension_id,
        api: this._xdm.getApiSpec(),
        origin: window.location.origin
      };

      return {
        id: extension_id,
        name: JSON.stringify(data),
        src: extension.url
      };
    }
  }, {
    key: 'registerExtension',
    value: function registerExtension(extension, initCallback) {
      var extension_id = this._createId(extension);
      this._xdm.registerExtension(extension_id, {
        extension: extension,
        initCallback: initCallback
      });
      return extension_id;
    }
  }, {
    key: 'defineModule',
    value: function defineModule(moduleName, module) {
      this._xdm.defineAPIModule(module, moduleName);
    }
  }, {
    key: 'defineGlobals',
    value: function defineGlobals(module) {
      this._xdm.defineAPIModule(module);
    }
  }, {
    key: 'getExtensions',
    value: function getExtensions(filter) {
      return this._xdm.getRegisteredExtensions(filter);
    }
  }, {
    key: 'unregisterExtension',
    value: function unregisterExtension(filter) {
      return this._xdm.unregisterExtension(filter);
    }
  }]);

  return Connect;
})();

module.exports = new Connect();

},{"../common/util":3,"./xdmrpc":4}],2:[function(_dereq_,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _util = _dereq_('./util');

var _util2 = _interopRequireDefault(_util);

var PostMessage = (function () {
  function PostMessage(data) {
    _classCallCheck(this, PostMessage);

    var d = data || {};
    this._registerListener(d.listenOn);
  }

  // listen for postMessage events (defaults to window).

  _createClass(PostMessage, [{
    key: "_registerListener",
    value: function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }
      listenOn.addEventListener("message", _util2["default"]._bind(this, this._receiveMessage), false);
    }
  }, {
    key: "_receiveMessage",
    value: function _receiveMessage(event) {
      var extensionId = event.data.eid,
          reg = undefined;

      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }

      if (!this._checkOrigin(event, reg)) {
        return false;
      }

      var handler = this._messageHandlers[event.data.type];
      if (handler) {
        handler.call(this, event, reg);
      }
    }
  }]);

  return PostMessage;
})();

module.exports = PostMessage;

},{"./util":3}],3:[function(_dereq_,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LOG_PREFIX = "[Simple-XDM] ";

var Util = (function () {
  function Util() {
    _classCallCheck(this, Util);
  }

  _createClass(Util, [{
    key: "randomString",
    value: function randomString() {
      return Math.floor(Math.random() * 1000000000).toString(16);
    }

    // might be un-needed
  }, {
    key: "argumentsToArray",
    value: function argumentsToArray(arrayLike) {
      var array = [];
      for (var i = 0; i < arrayLike.length; i++) {
        array.push(arrayLike[i]);
      }
      return array;
    }
  }, {
    key: "hasCallback",
    value: function hasCallback(args) {
      var length = args.length;
      return length > 0 && typeof args[length - 1] === 'function';
    }
  }, {
    key: "error",
    value: function error(msg) {
      console.error(LOG_PREFIX + msg);
    }
  }, {
    key: "warn",
    value: function warn(msg) {
      console.warn(LOG_PREFIX + msg);
    }
  }, {
    key: "_bind",
    value: function _bind(thisp, fn) {
      if (Function.prototype.bind) {
        return fn.bind(thisp);
      }
      return function () {
        return fn.apply(thisp, arguments);
      };
    }
  }]);

  return Util;
})();

module.exports = new Util();

},{}],4:[function(_dereq_,module,exports){
/**
* Postmessage format:
*
* Initialization
* --------------
* {
*   type: 'init',
*   eid: 'my-addon__my-module-xyz'  // the extension identifier, unique across iframes
* }
*
* Request
* -------
* {
*   type: 'req',
*   eid: 'my-addon__my-module-xyz',  // the extension identifier, unique for iframe
*   mid: 'xyz',  // a unique message identifier, required for callbacks
*   mod: 'cookie',  // the module name
*   fn: 'read',  // the method name
*   args: [arguments]  // the method arguments
* }
*
* Response
* --------
* {
*   type: 'resp'
*   eid: 'my-addon__my-module-xyz',  // the extension identifier, unique for iframe
*   mid: 'xyz',  // a unique message identifier, obtained from the request
*   args: [arguments]  // the callback arguments
* }
*
* Event
* -----
* {
*   type: 'evt',
*   etyp: 'some-event',
*   evnt: { ... }  // the event data
*   mid: 'xyz', // a unique message identifier for the event
* }
**/

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _commonUtil = _dereq_('../common/util');

var _commonUtil2 = _interopRequireDefault(_commonUtil);

var _commonPostmessage = _dereq_('../common/postmessage');

var _commonPostmessage2 = _interopRequireDefault(_commonPostmessage);

var VALID_EVENT_TIME_MS = 30000; //30 seconds

var XDMRPC = (function (_PostMessage) {
  _inherits(XDMRPC, _PostMessage);

  function XDMRPC(config) {
    _classCallCheck(this, XDMRPC);

    config = config || {};
    _get(Object.getPrototypeOf(XDMRPC.prototype), 'constructor', this).call(this, config);
    this._registeredExtensions = config.extensions || {};
    this._registeredAPIModules = {};
    this._pendingCallbacks = {};
    this._pendingEvents = {};
    this._messageHandlers = {
      init: this._handleInit,
      req: this._handleRequest,
      resp: this._handleResponse,
      event_query: this._handleEventQuery
    };
  }

  _createClass(XDMRPC, [{
    key: '_handleInit',
    value: function _handleInit(event, reg) {
      this._registeredExtensions[reg.extension_id].source = event.source;
      if (reg.initCallback) {
        reg.initCallback(event.data.eid);
        delete reg.initCallback;
      }
    }
  }, {
    key: '_handleResponse',
    value: function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];
      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        pendingCallback.apply(window, data.args);
      }
    }
  }, {
    key: '_handleRequest',
    value: function _handleRequest(event, reg) {
      function sendResponse() {
        var args = _commonUtil2['default'].argumentsToArray(arguments);
        event.source.postMessage({
          mid: event.data.mid,
          type: 'resp',
          args: args
        }, reg.extension.url);
      }

      var data = event.data;
      var module = this._registeredAPIModules[data.mod];
      if (module) {
        var method = module[data.fn];
        if (method) {
          var methodArgs = data.args;
          sendResponse._context = {
            addon_key: reg.extension.addon_key,
            key: reg.extension.key
          };
          methodArgs.push(sendResponse);
          method.apply(module, methodArgs);
        }
      }
    }
  }, {
    key: 'defineAPIModule',
    value: function defineAPIModule(module, moduleName) {
      if (!moduleName) {
        this._registeredAPIModules._globals = module;
      }
      this._registeredAPIModules[moduleName] = module;
      return this._registeredAPIModules;
    }
  }, {
    key: '_fullKey',
    value: function _fullKey(targetSpec) {
      var key = targetSpec.addon_key || 'global';
      if (targetSpec.key) {
        key = key + '@@' + targetSpec.key;
      }

      return key;
    }
  }, {
    key: 'queueEvent',
    value: function queueEvent(type, targetSpec, event, callback) {
      var loaded_frame,
          targets = this._findRegistrations(targetSpec);

      loaded_frame = targets.some(function (target) {
        return target.registered_events !== undefined;
      }, this);

      if (loaded_frame) {
        this.dispatch(type, targetSpec, event, callback);
      } else {
        this._pendingEvents[this._fullKey(targetSpec)] = {
          type: type,
          targetSpec: targetSpec,
          event: event,
          callback: callback,
          time: new Date().getTime(),
          uid: _commonUtil2['default'].randomString()
        };
      }
    }
  }, {
    key: '_handleEventQuery',
    value: function _handleEventQuery(message, extension) {
      var _this = this;

      var executed = {};
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this._pendingEvents[index];
        if (now - element.time <= VALID_EVENT_TIME_MS) {
          executed[index] = element;
          element.targetSpec.addon_key = extension.extension.addon_key;
          element.targetSpec.key = extension.extension.key;
          _this.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
        }
        delete _this._pendingEvents[index];
      });

      this._registeredExtensions[extension.extension_id].registered_events = message.data.args;

      return executed;
    }
  }, {
    key: 'dispatch',
    value: function dispatch(type, targetSpec, event, callback, source) {
      function sendEvent(reg, evnt) {
        if (reg.source) {
          var mid;
          if (callback) {
            mid = _commonUtil2['default'].randomString();
            this._pendingCallbacks[mid] = callback;
          }

          reg.source.postMessage({
            type: 'evt',
            mid: mid,
            etyp: type,
            evnt: evnt
          }, reg.extension.url);
        } else {
          throw "Cannot send post message without a source";
        }
      }

      var registrations = this._findRegistrations(targetSpec || {});
      registrations.forEach(function (reg) {
        if (source) {
          reg.source = source;
        }
        _commonUtil2['default']._bind(this, sendEvent)(reg, event);
      }, this);
    }
  }, {
    key: '_findRegistrations',
    value: function _findRegistrations(targetSpec) {
      var _this2 = this;

      if (this._registeredExtensions.length === 0) {
        _commonUtil2['default'].error('no registered extensions', this._registeredExtensions);
        return [];
      }
      var keys = Object.getOwnPropertyNames(targetSpec);
      var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map(function (key) {
        return _this2._registeredExtensions[key];
      });

      return registrations.filter(function (reg) {
        return keys.every(function (key) {
          return reg.extension[key] === targetSpec[key];
        });
      });
    }
  }, {
    key: 'registerExtension',
    value: function registerExtension(extension_id, data) {
      // delete duplicate registrations
      if (data.extension.addon_key && data.extension.key) {
        var existingView = this._findRegistrations({
          addon_key: data.extension.addon_key,
          key: data.extension.key
        });
        if (existingView.length !== 0) {
          delete this._registeredExtensions[existingView[0].extension_id];
        }
      }
      data.extension_id = extension_id;
      this._registeredExtensions[extension_id] = data;
    }
  }, {
    key: 'getApiSpec',
    value: function getApiSpec() {
      var that = this;
      function createModule(moduleName) {
        var module = that._registeredAPIModules[moduleName];
        if (!module) {
          throw new Error("unregistered API module: " + moduleName);
        }
        return Object.getOwnPropertyNames(module).reduce(function (accumulator, functionName) {
          if (typeof module[functionName] === 'function') {
            accumulator[functionName] = {}; // could hold function metadata, empty for now
          }
          return accumulator;
        }, {});
      }

      return Object.getOwnPropertyNames(this._registeredAPIModules).reduce(function (accumulator, moduleName) {
        accumulator[moduleName] = createModule(moduleName);
        return accumulator;
      }, {});
    }

    // validate origin of postMessage
  }, {
    key: '_checkOrigin',
    value: function _checkOrigin(event, reg) {
      var no_source_types = ['init', 'event_query'];
      var isValidOrigin = reg && reg.extension.url.indexOf(event.origin) === 0 && (!reg.source && no_source_types.indexOf(event.data.type) > -1 || event.source === reg.source);
      if (!isValidOrigin) {
        _commonUtil2['default'].warn("Failed to validate origin: " + event.origin);
      }
      return isValidOrigin;
    }
  }, {
    key: 'getRegisteredExtensions',
    value: function getRegisteredExtensions(filter) {
      if (filter) {
        return this._findRegistrations(filter);
      }
      return this._registeredExtensions;
    }
  }, {
    key: 'unregisterExtension',
    value: function unregisterExtension(filter) {
      var registrations = this._findRegistrations(filter);
      if (registrations.length !== 0) {
        registrations.forEach(function (registration) {
          delete this._registeredExtensions[registration.extension_id];
        }, this);
      }
    }
  }]);

  return XDMRPC;
})(_commonPostmessage2['default']);

module.exports = XDMRPC;

},{"../common/postmessage":2,"../common/util":3}]},{},[1])(1)
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvc3R1ZmYvaGlwY2hhdC9zaW1wbGUteGRtL3NyYy9ob3N0L2luZGV4LmpzIiwiL3N0dWZmL2hpcGNoYXQvc2ltcGxlLXhkbS9zcmMvY29tbW9uL3Bvc3RtZXNzYWdlLmpzIiwiL3N0dWZmL2hpcGNoYXQvc2ltcGxlLXhkbS9zcmMvY29tbW9uL3V0aWwuanMiLCIvc3R1ZmYvaGlwY2hhdC9zaW1wbGUteGRtL3NyYy9ob3N0L3hkbXJwYy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7O3NCQ0FtQixVQUFVOzs7OzBCQUNYLGdCQUFnQjs7OztJQUU1QixPQUFPO0FBRUEsV0FGUCxPQUFPLEdBRUc7MEJBRlYsT0FBTzs7QUFHVCxRQUFJLENBQUMsSUFBSSxHQUFHLHlCQUFZLENBQUM7R0FDMUI7O2VBSkcsT0FBTzs7V0FNSCxrQkFBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUU7QUFDMUMsVUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFDeEQsYUFBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0tBQ3ZDOzs7V0FFUSxtQkFBQyxTQUFTLEVBQUU7QUFDbkIsVUFBRyxDQUFDLFNBQVMsQ0FBQyxTQUFTLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFDO0FBQ3hDLGNBQU0sS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7T0FDckQ7QUFDRCxhQUFPLFNBQVMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxHQUFHLFNBQVMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxHQUFHLHdCQUFNLFlBQVksRUFBRSxDQUFDO0tBQ2pGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7V0FlSyxnQkFBQyxTQUFTLEVBQUUsWUFBWSxFQUFFO0FBQzlCLFVBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsWUFBWSxDQUFDLENBQUM7O0FBRW5FLFVBQUksSUFBSSxHQUFHO0FBQ1Qsb0JBQVksRUFBRSxZQUFZO0FBQzFCLFdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtBQUMzQixjQUFNLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNO09BQy9CLENBQUM7O0FBRUYsYUFBTztBQUNMLFVBQUUsRUFBRSxZQUFZO0FBQ2hCLFlBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztBQUMxQixXQUFHLEVBQUUsU0FBUyxDQUFDLEdBQUc7T0FDbkIsQ0FBQztLQUNIOzs7V0FFZ0IsMkJBQUMsU0FBUyxFQUFFLFlBQVksRUFBRTtBQUN6QyxVQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQzdDLFVBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFO0FBQ3hDLGlCQUFTLEVBQUUsU0FBUztBQUNwQixvQkFBWSxFQUFFLFlBQVk7T0FDM0IsQ0FBQyxDQUFDO0FBQ0gsYUFBTyxZQUFZLENBQUM7S0FDckI7OztXQUVXLHNCQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUU7QUFDL0IsVUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0tBQy9DOzs7V0FFWSx1QkFBQyxNQUFNLEVBQUU7QUFDcEIsVUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDbkM7OztXQUVZLHVCQUFDLE1BQU0sRUFBRTtBQUNwQixhQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDbEQ7OztXQUVrQiw2QkFBQyxNQUFNLEVBQUU7QUFDMUIsYUFBTyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQzlDOzs7U0F0RUcsT0FBTzs7O0FBMEViLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7b0JDN0VkLFFBQVE7Ozs7SUFDbkIsV0FBVztBQUVKLFdBRlAsV0FBVyxDQUVILElBQUksRUFBRTswQkFGZCxXQUFXOztBQUdiLFFBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7QUFDbkIsUUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztHQUNwQzs7OztlQUxHLFdBQVc7O1dBUUUsMkJBQUMsUUFBUSxFQUFFO0FBQzFCLFVBQUcsQ0FBQyxRQUFRLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUU7QUFDMUMsZ0JBQVEsR0FBRyxNQUFNLENBQUM7T0FDbkI7QUFDRCxjQUFRLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLGtCQUFLLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQ3JGOzs7V0FFZSx5QkFBQyxLQUFLLEVBQUU7QUFDdEIsVUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHO1VBQ2hDLEdBQUcsWUFBQSxDQUFDOztBQUVKLFVBQUcsV0FBVyxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBQztBQUMzQyxXQUFHLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxDQUFDO09BQy9DOztBQUVELFVBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsRUFBRTtBQUNsQyxlQUFPLEtBQUssQ0FBQztPQUNkOztBQUVELFVBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3JELFVBQUksT0FBTyxFQUFFO0FBQ1gsZUFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO09BQ2hDO0tBQ0Y7OztTQS9CRyxXQUFXOzs7QUFtQ2pCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDOzs7Ozs7Ozs7QUNwQzdCLElBQU0sVUFBVSxHQUFHLGVBQWUsQ0FBQzs7SUFFN0IsSUFBSTtXQUFKLElBQUk7MEJBQUosSUFBSTs7O2VBQUosSUFBSTs7V0FDSSx3QkFBRztBQUNiLGFBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsVUFBVSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQzVEOzs7OztXQUVlLDBCQUFDLFNBQVMsRUFBRTtBQUMxQixVQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7QUFDZixXQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUN6QyxhQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO09BQzFCO0FBQ0QsYUFBTyxLQUFLLENBQUM7S0FDZDs7O1dBRVUscUJBQUMsSUFBSSxFQUFFO0FBQ2hCLFVBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7QUFDekIsYUFBTyxNQUFNLEdBQUcsQ0FBQyxJQUFJLE9BQU8sSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxVQUFVLENBQUM7S0FDN0Q7OztXQUVJLGVBQUMsR0FBRyxFQUFFO0FBQ1AsYUFBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUM7S0FDbkM7OztXQUVHLGNBQUMsR0FBRyxFQUFFO0FBQ04sYUFBTyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUM7S0FDbEM7OztXQUVJLGVBQUMsS0FBSyxFQUFFLEVBQUUsRUFBQztBQUNkLFVBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUU7QUFDMUIsZUFBTyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO09BQ3ZCO0FBQ0QsYUFBTyxZQUFZO0FBQ2pCLGVBQU8sRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7T0FDbkMsQ0FBQztLQUNIOzs7U0FqQ0csSUFBSTs7O0FBdUNWLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzswQkNEVixnQkFBZ0I7Ozs7aUNBQ1YsdUJBQXVCOzs7O0FBRS9DLElBQUksbUJBQW1CLEdBQUcsS0FBSyxDQUFDOztJQUUxQixNQUFNO1lBQU4sTUFBTTs7QUFFQyxXQUZQLE1BQU0sQ0FFRSxNQUFNLEVBQUU7MEJBRmhCLE1BQU07O0FBR1IsVUFBTSxHQUFHLE1BQU0sSUFBSSxFQUFFLENBQUM7QUFDdEIsK0JBSkUsTUFBTSw2Q0FJRixNQUFNLEVBQUU7QUFDZCxRQUFJLENBQUMscUJBQXFCLEdBQUcsTUFBTSxDQUFDLFVBQVUsSUFBSSxFQUFFLENBQUM7QUFDckQsUUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztBQUNoQyxRQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO0FBQzVCLFFBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO0FBQ3pCLFFBQUksQ0FBQyxnQkFBZ0IsR0FBRztBQUN0QixVQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVc7QUFDdEIsU0FBRyxFQUFFLElBQUksQ0FBQyxjQUFjO0FBQ3hCLFVBQUksRUFBRSxJQUFJLENBQUMsZUFBZTtBQUMxQixpQkFBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUI7S0FDcEMsQ0FBQztHQUNIOztlQWZHLE1BQU07O1dBaUJDLHFCQUFDLEtBQUssRUFBRSxHQUFHLEVBQUU7QUFDdEIsVUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztBQUNuRSxVQUFJLEdBQUcsQ0FBQyxZQUFZLEVBQUU7QUFDcEIsV0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ2pDLGVBQU8sR0FBRyxDQUFDLFlBQVksQ0FBQztPQUN6QjtLQUNGOzs7V0FFYyx5QkFBQyxLQUFLLEVBQUU7QUFDckIsVUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztBQUN0QixVQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3ZELFVBQUksZUFBZSxFQUFFO0FBQ25CLGVBQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUN4Qyx1QkFBZSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO09BQzFDO0tBQ0Y7OztXQUVhLHdCQUFDLEtBQUssRUFBRSxHQUFHLEVBQUU7QUFDekIsZUFBUyxZQUFZLEdBQUc7QUFDdEIsWUFBSSxJQUFJLEdBQUcsd0JBQU0sZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDN0MsYUFBSyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7QUFDdkIsYUFBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRztBQUNuQixjQUFJLEVBQUUsTUFBTTtBQUNaLGNBQUksRUFBRSxJQUFJO1NBQ1gsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO09BQ3ZCOztBQUVELFVBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7QUFDdEIsVUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNsRCxVQUFJLE1BQU0sRUFBRTtBQUNWLFlBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDN0IsWUFBSSxNQUFNLEVBQUU7QUFDVixjQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQzNCLHNCQUFZLENBQUMsUUFBUSxHQUFHO0FBQ3RCLHFCQUFTLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxTQUFTO0FBQ2xDLGVBQUcsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUc7V0FDdkIsQ0FBQztBQUNGLG9CQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0FBQzlCLGdCQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNsQztPQUNGO0tBQ0Y7OztXQUdjLHlCQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUM7QUFDakMsVUFBRyxDQUFDLFVBQVUsRUFBQztBQUNiLFlBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDO09BQzlDO0FBQ0QsVUFBSSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxHQUFHLE1BQU0sQ0FBQztBQUNoRCxhQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztLQUNuQzs7O1dBRU8sa0JBQUMsVUFBVSxFQUFDO0FBQ2xCLFVBQUksR0FBRyxHQUFHLFVBQVUsQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDO0FBQzNDLFVBQUcsVUFBVSxDQUFDLEdBQUcsRUFBQztBQUNoQixXQUFHLEdBQU0sR0FBRyxVQUFLLFVBQVUsQ0FBQyxHQUFHLEFBQUUsQ0FBQztPQUNuQzs7QUFFRCxhQUFPLEdBQUcsQ0FBQztLQUNaOzs7V0FFUyxvQkFBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUU7QUFDNUMsVUFBSSxZQUFZO1VBQ2hCLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLENBQUM7O0FBRTlDLGtCQUFZLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUN0QyxlQUFPLE1BQU0sQ0FBQyxpQkFBaUIsS0FBSyxTQUFTLENBQUM7T0FDL0MsRUFBRSxJQUFJLENBQUMsQ0FBQzs7QUFFVCxVQUFHLFlBQVksRUFBQztBQUNkLFlBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7T0FDbEQsTUFBTTtBQUNMLFlBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHO0FBQy9DLGNBQUksRUFBSixJQUFJO0FBQ0osb0JBQVUsRUFBVixVQUFVO0FBQ1YsZUFBSyxFQUFMLEtBQUs7QUFDTCxrQkFBUSxFQUFSLFFBQVE7QUFDUixjQUFJLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUU7QUFDMUIsYUFBRyxFQUFFLHdCQUFNLFlBQVksRUFBRTtTQUMxQixDQUFDO09BQ0g7S0FDRjs7O1dBRWdCLDJCQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUU7OztBQUNwQyxVQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7QUFDbEIsVUFBSSxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUMvQixVQUFJLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUM1QyxVQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ3RCLFlBQUksT0FBTyxHQUFHLE1BQUssY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ3pDLFlBQUksQUFBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSyxtQkFBbUIsRUFBRTtBQUMvQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLE9BQU8sQ0FBQztBQUMxQixpQkFBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7QUFDN0QsaUJBQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDO0FBQ2pELGdCQUFLLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNsRztBQUNELGVBQU8sTUFBSyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7T0FDbkMsQ0FBQyxDQUFDOztBQUVILFVBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7O0FBRXpGLGFBQU8sUUFBUSxDQUFDO0tBQ2pCOzs7V0FFTyxrQkFBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO0FBQ2xELGVBQVMsU0FBUyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUU7QUFDNUIsWUFBSSxHQUFHLENBQUMsTUFBTSxFQUFFO0FBQ2QsY0FBSSxHQUFHLENBQUM7QUFDUixjQUFJLFFBQVEsRUFBRTtBQUNaLGVBQUcsR0FBRyx3QkFBTSxZQUFZLEVBQUUsQ0FBQztBQUMzQixnQkFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxHQUFHLFFBQVEsQ0FBQztXQUN4Qzs7QUFFRCxhQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztBQUNyQixnQkFBSSxFQUFFLEtBQUs7QUFDWCxlQUFHLEVBQUUsR0FBRztBQUNSLGdCQUFJLEVBQUUsSUFBSTtBQUNWLGdCQUFJLEVBQUUsSUFBSTtXQUNYLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN2QixNQUFNO0FBQ0wsZ0JBQU0sMkNBQTJDLENBQUM7U0FDbkQ7T0FDRjs7QUFFRCxVQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBQzlELG1CQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFO0FBQ25DLFlBQUcsTUFBTSxFQUFDO0FBQ1IsYUFBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDckI7QUFDRCxnQ0FBTSxLQUFLLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztPQUMxQyxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ1Y7OztXQUVpQiw0QkFBQyxVQUFVLEVBQUU7OztBQUM3QixVQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFDO0FBQ3pDLGdDQUFNLEtBQUssQ0FBQywwQkFBMEIsRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztBQUNwRSxlQUFPLEVBQUUsQ0FBQztPQUNYO0FBQ0QsVUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQ2xELFVBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHLEVBQUs7QUFDdEYsZUFBTyxPQUFLLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO09BQ3hDLENBQUMsQ0FBQzs7QUFFSCxhQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLEVBQUU7QUFDekMsZUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxFQUFFO0FBQy9CLGlCQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEtBQUssVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQy9DLENBQUMsQ0FBQztPQUNKLENBQUMsQ0FBQztLQUNKOzs7V0FFZ0IsMkJBQUMsWUFBWSxFQUFFLElBQUksRUFBRTs7QUFFcEMsVUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBQztBQUNoRCxZQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7QUFDekMsbUJBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVM7QUFDbkMsYUFBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRztTQUN4QixDQUFDLENBQUM7QUFDSCxZQUFHLFlBQVksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFDO0FBQzNCLGlCQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDakU7T0FDRjtBQUNELFVBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO0FBQ2pDLFVBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUM7S0FDakQ7OztXQUVTLHNCQUFHO0FBQ1gsVUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2hCLGVBQVMsWUFBWSxDQUFDLFVBQVUsRUFBRTtBQUNoQyxZQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDcEQsWUFBRyxDQUFDLE1BQU0sRUFBQztBQUNULGdCQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixHQUFHLFVBQVUsQ0FBQyxDQUFDO1NBQzNEO0FBQ0QsZUFBTyxNQUFNLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsV0FBVyxFQUFFLFlBQVksRUFBSztBQUM1RSxjQUFJLE9BQU8sTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLFVBQVUsRUFBRTtBQUM1Qyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsQ0FBQztXQUNsQztBQUNELGlCQUFPLFdBQVcsQ0FBQztTQUN0QixFQUFFLEVBQUUsQ0FBQyxDQUFDO09BQ1I7O0FBRUQsYUFBTyxNQUFNLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsV0FBVyxFQUFFLFVBQVUsRUFBSztBQUM5RixtQkFBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUNuRCxlQUFPLFdBQVcsQ0FBQztPQUN0QixFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQ1I7Ozs7O1dBSVcsc0JBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRTtBQUN2QixVQUFJLGVBQWUsR0FBRyxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQztBQUM5QyxVQUFJLGFBQWEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQ25FLEFBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxJQUFJLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSyxLQUFLLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUEsQUFBQyxDQUFDO0FBQ3BHLFVBQUcsQ0FBQyxhQUFhLEVBQUU7QUFDZixnQ0FBTSxJQUFJLENBQUMsNkJBQTZCLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO09BQzVEO0FBQ0QsYUFBTyxhQUFhLENBQUM7S0FDdEI7OztXQUVzQixpQ0FBQyxNQUFNLEVBQUU7QUFDOUIsVUFBRyxNQUFNLEVBQUU7QUFDVCxlQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztPQUN4QztBQUNELGFBQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDO0tBQ25DOzs7V0FFa0IsNkJBQUMsTUFBTSxFQUFFO0FBQzFCLFVBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUNwRCxVQUFHLGFBQWEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFDO0FBQzVCLHFCQUFhLENBQUMsT0FBTyxDQUFDLFVBQVMsWUFBWSxFQUFFO0FBQzNDLGlCQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDOUQsRUFBRSxJQUFJLENBQUMsQ0FBQztPQUNWO0tBQ0Y7OztTQXBPRyxNQUFNOzs7QUF3T1osTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiaW1wb3J0IFhETVJQQyBmcm9tICcuL3hkbXJwYyc7XG5pbXBvcnQgVXRpbHMgZnJvbSAnLi4vY29tbW9uL3V0aWwnO1xuXG5jbGFzcyBDb25uZWN0IHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLl94ZG0gPSBuZXcgWERNUlBDKCk7XG4gIH1cblxuICBkaXNwYXRjaCh0eXBlLCB0YXJnZXRTcGVjLCBldmVudCwgY2FsbGJhY2spIHtcbiAgICB0aGlzLl94ZG0ucXVldWVFdmVudCh0eXBlLCB0YXJnZXRTcGVjLCBldmVudCwgY2FsbGJhY2spO1xuICAgIHJldHVybiB0aGlzLmdldEV4dGVuc2lvbnModGFyZ2V0U3BlYyk7XG4gIH1cblxuICBfY3JlYXRlSWQoZXh0ZW5zaW9uKSB7XG4gICAgaWYoIWV4dGVuc2lvbi5hZGRvbl9rZXkgfHwgIWV4dGVuc2lvbi5rZXkpe1xuICAgICAgdGhyb3cgRXJyb3IoJ0V4dGVuc2lvbnMgcmVxdWlyZSBhZGRvbl9rZXkgYW5kIGtleScpO1xuICAgIH1cbiAgICByZXR1cm4gZXh0ZW5zaW9uLmFkZG9uX2tleSArICdfXycgKyBleHRlbnNpb24ua2V5ICsgJ19fJyArIFV0aWxzLnJhbmRvbVN0cmluZygpO1xuICB9XG4gIC8qKlxuICAqIENyZWF0ZXMgYSBuZXcgaWZyYW1lZCBtb2R1bGUsIHdpdGhvdXQgYWN0dWFsbHkgY3JlYXRpbmcgdGhlIERPTSBlbGVtZW50LlxuICAqIFRoZSBpZnJhbWUgYXR0cmlidXRlcyBhcmUgcGFzc2VkIHRvIHRoZSAnc2V0dXBDYWxsYmFjaycsIHdoaWNoIGlzIHJlc3BvbnNpYmxlIGZvciBjcmVhdGluZ1xuICAqIHRoZSBET00gZWxlbWVudCBhbmQgcmV0dXJuaW5nIHRoZSB3aW5kb3cgcmVmZXJlbmNlLlxuICAqXG4gICogQHBhcmFtIGV4dGVuc2lvbiBUaGUgZXh0ZW5zaW9uIGRlZmluaXRpb24uIEV4YW1wbGU6XG4gICogICB7XG4gICogICAgIGFkZG9uS2V5OiAnbXktYWRkb24nLFxuICAqICAgICBtb2R1bGVLZXk6ICdteS1tb2R1bGUnLFxuICAqICAgICB1cmw6ICdodHRwczovL2V4YW1wbGUuY29tL215LW1vZHVsZSdcbiAgKiAgIH1cbiAgKlxuICAqIEBwYXJhbSBpbml0Q2FsbGJhY2sgVGhlIG9wdGlvbmFsIGluaXRDYWxsYmFjayBpcyBjYWxsZWQgd2hlbiB0aGUgYnJpZGdlIGJldHdlZW4gaG9zdCBhbmQgaWZyYW1lIGlzIGVzdGFibGlzaGVkLlxuICAqKi9cbiAgY3JlYXRlKGV4dGVuc2lvbiwgaW5pdENhbGxiYWNrKSB7XG4gICAgbGV0IGV4dGVuc2lvbl9pZCA9IHRoaXMucmVnaXN0ZXJFeHRlbnNpb24oZXh0ZW5zaW9uLCBpbml0Q2FsbGJhY2spO1xuXG4gICAgbGV0IGRhdGEgPSB7XG4gICAgICBleHRlbnNpb25faWQ6IGV4dGVuc2lvbl9pZCxcbiAgICAgIGFwaTogdGhpcy5feGRtLmdldEFwaVNwZWMoKSxcbiAgICAgIG9yaWdpbjogd2luZG93LmxvY2F0aW9uLm9yaWdpblxuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgaWQ6IGV4dGVuc2lvbl9pZCxcbiAgICAgIG5hbWU6IEpTT04uc3RyaW5naWZ5KGRhdGEpLFxuICAgICAgc3JjOiBleHRlbnNpb24udXJsXG4gICAgfTtcbiAgfVxuXG4gIHJlZ2lzdGVyRXh0ZW5zaW9uKGV4dGVuc2lvbiwgaW5pdENhbGxiYWNrKSB7XG4gICAgbGV0IGV4dGVuc2lvbl9pZCA9IHRoaXMuX2NyZWF0ZUlkKGV4dGVuc2lvbik7XG4gICAgdGhpcy5feGRtLnJlZ2lzdGVyRXh0ZW5zaW9uKGV4dGVuc2lvbl9pZCwge1xuICAgICAgZXh0ZW5zaW9uOiBleHRlbnNpb24sXG4gICAgICBpbml0Q2FsbGJhY2s6IGluaXRDYWxsYmFja1xuICAgIH0pO1xuICAgIHJldHVybiBleHRlbnNpb25faWQ7XG4gIH1cblxuICBkZWZpbmVNb2R1bGUobW9kdWxlTmFtZSwgbW9kdWxlKSB7XG4gICAgdGhpcy5feGRtLmRlZmluZUFQSU1vZHVsZShtb2R1bGUsIG1vZHVsZU5hbWUpO1xuICB9XG5cbiAgZGVmaW5lR2xvYmFscyhtb2R1bGUpIHtcbiAgICB0aGlzLl94ZG0uZGVmaW5lQVBJTW9kdWxlKG1vZHVsZSk7XG4gIH1cblxuICBnZXRFeHRlbnNpb25zKGZpbHRlcikge1xuICAgIHJldHVybiB0aGlzLl94ZG0uZ2V0UmVnaXN0ZXJlZEV4dGVuc2lvbnMoZmlsdGVyKTtcbiAgfVxuXG4gIHVucmVnaXN0ZXJFeHRlbnNpb24oZmlsdGVyKSB7XG4gICAgcmV0dXJuIHRoaXMuX3hkbS51bnJlZ2lzdGVyRXh0ZW5zaW9uKGZpbHRlcik7XG4gIH1cblxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IG5ldyBDb25uZWN0KCk7IiwiaW1wb3J0IFV0aWwgZnJvbSAnLi91dGlsJztcbmNsYXNzIFBvc3RNZXNzYWdlIHtcblxuICBjb25zdHJ1Y3RvcihkYXRhKSB7XG4gICAgbGV0IGQgPSBkYXRhIHx8IHt9O1xuICAgIHRoaXMuX3JlZ2lzdGVyTGlzdGVuZXIoZC5saXN0ZW5Pbik7XG4gIH1cblxuIC8vIGxpc3RlbiBmb3IgcG9zdE1lc3NhZ2UgZXZlbnRzIChkZWZhdWx0cyB0byB3aW5kb3cpLlxuICBfcmVnaXN0ZXJMaXN0ZW5lcihsaXN0ZW5Pbikge1xuICAgIGlmKCFsaXN0ZW5PbiB8fCAhbGlzdGVuT24uYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgICAgbGlzdGVuT24gPSB3aW5kb3c7XG4gICAgfVxuICAgIGxpc3Rlbk9uLmFkZEV2ZW50TGlzdGVuZXIoXCJtZXNzYWdlXCIsIFV0aWwuX2JpbmQodGhpcywgdGhpcy5fcmVjZWl2ZU1lc3NhZ2UpLCBmYWxzZSk7XG4gIH1cblxuICBfcmVjZWl2ZU1lc3NhZ2UgKGV2ZW50KSB7XG4gICAgbGV0IGV4dGVuc2lvbklkID0gZXZlbnQuZGF0YS5laWQsXG4gICAgcmVnO1xuXG4gICAgaWYoZXh0ZW5zaW9uSWQgJiYgdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnMpe1xuICAgICAgcmVnID0gdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnNbZXh0ZW5zaW9uSWRdO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5fY2hlY2tPcmlnaW4oZXZlbnQsIHJlZykpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICB2YXIgaGFuZGxlciA9IHRoaXMuX21lc3NhZ2VIYW5kbGVyc1tldmVudC5kYXRhLnR5cGVdO1xuICAgIGlmIChoYW5kbGVyKSB7XG4gICAgICBoYW5kbGVyLmNhbGwodGhpcywgZXZlbnQsIHJlZyk7XG4gICAgfVxuICB9XG5cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBQb3N0TWVzc2FnZTsiLCJjb25zdCBMT0dfUFJFRklYID0gXCJbU2ltcGxlLVhETV0gXCI7XG5cbmNsYXNzIFV0aWwge1xuICByYW5kb21TdHJpbmcoKSB7XG4gICAgcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDEwMDAwMDAwMDApLnRvU3RyaW5nKDE2KTtcbiAgfVxuICAvLyBtaWdodCBiZSB1bi1uZWVkZWRcbiAgYXJndW1lbnRzVG9BcnJheShhcnJheUxpa2UpIHtcbiAgICB2YXIgYXJyYXkgPSBbXTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFycmF5TGlrZS5sZW5ndGg7IGkrKykge1xuICAgICAgYXJyYXkucHVzaChhcnJheUxpa2VbaV0pO1xuICAgIH1cbiAgICByZXR1cm4gYXJyYXk7XG4gIH1cblxuICBoYXNDYWxsYmFjayhhcmdzKSB7XG4gICAgdmFyIGxlbmd0aCA9IGFyZ3MubGVuZ3RoO1xuICAgIHJldHVybiBsZW5ndGggPiAwICYmIHR5cGVvZiBhcmdzW2xlbmd0aCAtIDFdID09PSAnZnVuY3Rpb24nO1xuICB9XG5cbiAgZXJyb3IobXNnKSB7XG4gICAgICBjb25zb2xlLmVycm9yKExPR19QUkVGSVggKyBtc2cpO1xuICB9XG5cbiAgd2Fybihtc2cpIHtcbiAgICAgIGNvbnNvbGUud2FybihMT0dfUFJFRklYICsgbXNnKTtcbiAgfVxuXG4gIF9iaW5kKHRoaXNwLCBmbil7XG4gICAgaWYoRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQpIHtcbiAgICAgIHJldHVybiBmbi5iaW5kKHRoaXNwKTtcbiAgICB9XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBmbi5hcHBseSh0aGlzcCwgYXJndW1lbnRzKTtcbiAgICB9O1xuICB9XG5cblxuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gbmV3IFV0aWwoKTsiLCIvKipcbiogUG9zdG1lc3NhZ2UgZm9ybWF0OlxuKlxuKiBJbml0aWFsaXphdGlvblxuKiAtLS0tLS0tLS0tLS0tLVxuKiB7XG4qICAgdHlwZTogJ2luaXQnLFxuKiAgIGVpZDogJ215LWFkZG9uX19teS1tb2R1bGUteHl6JyAgLy8gdGhlIGV4dGVuc2lvbiBpZGVudGlmaWVyLCB1bmlxdWUgYWNyb3NzIGlmcmFtZXNcbiogfVxuKlxuKiBSZXF1ZXN0XG4qIC0tLS0tLS1cbioge1xuKiAgIHR5cGU6ICdyZXEnLFxuKiAgIGVpZDogJ215LWFkZG9uX19teS1tb2R1bGUteHl6JywgIC8vIHRoZSBleHRlbnNpb24gaWRlbnRpZmllciwgdW5pcXVlIGZvciBpZnJhbWVcbiogICBtaWQ6ICd4eXonLCAgLy8gYSB1bmlxdWUgbWVzc2FnZSBpZGVudGlmaWVyLCByZXF1aXJlZCBmb3IgY2FsbGJhY2tzXG4qICAgbW9kOiAnY29va2llJywgIC8vIHRoZSBtb2R1bGUgbmFtZVxuKiAgIGZuOiAncmVhZCcsICAvLyB0aGUgbWV0aG9kIG5hbWVcbiogICBhcmdzOiBbYXJndW1lbnRzXSAgLy8gdGhlIG1ldGhvZCBhcmd1bWVudHNcbiogfVxuKlxuKiBSZXNwb25zZVxuKiAtLS0tLS0tLVxuKiB7XG4qICAgdHlwZTogJ3Jlc3AnXG4qICAgZWlkOiAnbXktYWRkb25fX215LW1vZHVsZS14eXonLCAgLy8gdGhlIGV4dGVuc2lvbiBpZGVudGlmaWVyLCB1bmlxdWUgZm9yIGlmcmFtZVxuKiAgIG1pZDogJ3h5eicsICAvLyBhIHVuaXF1ZSBtZXNzYWdlIGlkZW50aWZpZXIsIG9idGFpbmVkIGZyb20gdGhlIHJlcXVlc3RcbiogICBhcmdzOiBbYXJndW1lbnRzXSAgLy8gdGhlIGNhbGxiYWNrIGFyZ3VtZW50c1xuKiB9XG4qXG4qIEV2ZW50XG4qIC0tLS0tXG4qIHtcbiogICB0eXBlOiAnZXZ0JyxcbiogICBldHlwOiAnc29tZS1ldmVudCcsXG4qICAgZXZudDogeyAuLi4gfSAgLy8gdGhlIGV2ZW50IGRhdGFcbiogICBtaWQ6ICd4eXonLCAvLyBhIHVuaXF1ZSBtZXNzYWdlIGlkZW50aWZpZXIgZm9yIHRoZSBldmVudFxuKiB9XG4qKi9cblxuaW1wb3J0IFV0aWxzIGZyb20gJy4uL2NvbW1vbi91dGlsJztcbmltcG9ydCBQb3N0TWVzc2FnZSBmcm9tICcuLi9jb21tb24vcG9zdG1lc3NhZ2UnO1xuXG5sZXQgVkFMSURfRVZFTlRfVElNRV9NUyA9IDMwMDAwOyAvLzMwIHNlY29uZHNcblxuY2xhc3MgWERNUlBDIGV4dGVuZHMgUG9zdE1lc3NhZ2Uge1xuXG4gIGNvbnN0cnVjdG9yKGNvbmZpZykge1xuICAgIGNvbmZpZyA9IGNvbmZpZyB8fCB7fTtcbiAgICBzdXBlcihjb25maWcpO1xuICAgIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zID0gY29uZmlnLmV4dGVuc2lvbnMgfHwge307XG4gICAgdGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXMgPSB7fTtcbiAgICB0aGlzLl9wZW5kaW5nQ2FsbGJhY2tzID0ge307XG4gICAgdGhpcy5fcGVuZGluZ0V2ZW50cyA9IHt9O1xuICAgIHRoaXMuX21lc3NhZ2VIYW5kbGVycyA9IHtcbiAgICAgIGluaXQ6IHRoaXMuX2hhbmRsZUluaXQsXG4gICAgICByZXE6IHRoaXMuX2hhbmRsZVJlcXVlc3QsXG4gICAgICByZXNwOiB0aGlzLl9oYW5kbGVSZXNwb25zZSxcbiAgICAgIGV2ZW50X3F1ZXJ5OiB0aGlzLl9oYW5kbGVFdmVudFF1ZXJ5XG4gICAgfTtcbiAgfVxuXG4gIF9oYW5kbGVJbml0KGV2ZW50LCByZWcpIHtcbiAgICB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9uc1tyZWcuZXh0ZW5zaW9uX2lkXS5zb3VyY2UgPSBldmVudC5zb3VyY2U7XG4gICAgaWYgKHJlZy5pbml0Q2FsbGJhY2spIHtcbiAgICAgIHJlZy5pbml0Q2FsbGJhY2soZXZlbnQuZGF0YS5laWQpO1xuICAgICAgZGVsZXRlIHJlZy5pbml0Q2FsbGJhY2s7XG4gICAgfVxuICB9XG5cbiAgX2hhbmRsZVJlc3BvbnNlKGV2ZW50KSB7XG4gICAgdmFyIGRhdGEgPSBldmVudC5kYXRhO1xuICAgIHZhciBwZW5kaW5nQ2FsbGJhY2sgPSB0aGlzLl9wZW5kaW5nQ2FsbGJhY2tzW2RhdGEubWlkXTtcbiAgICBpZiAocGVuZGluZ0NhbGxiYWNrKSB7XG4gICAgICBkZWxldGUgdGhpcy5fcGVuZGluZ0NhbGxiYWNrc1tkYXRhLm1pZF07XG4gICAgICBwZW5kaW5nQ2FsbGJhY2suYXBwbHkod2luZG93LCBkYXRhLmFyZ3MpO1xuICAgIH1cbiAgfVxuXG4gIF9oYW5kbGVSZXF1ZXN0KGV2ZW50LCByZWcpIHtcbiAgICBmdW5jdGlvbiBzZW5kUmVzcG9uc2UoKSB7XG4gICAgICB2YXIgYXJncyA9IFV0aWxzLmFyZ3VtZW50c1RvQXJyYXkoYXJndW1lbnRzKTtcbiAgICAgIGV2ZW50LnNvdXJjZS5wb3N0TWVzc2FnZSh7XG4gICAgICAgIG1pZDogZXZlbnQuZGF0YS5taWQsXG4gICAgICAgIHR5cGU6ICdyZXNwJyxcbiAgICAgICAgYXJnczogYXJnc1xuICAgICAgfSwgcmVnLmV4dGVuc2lvbi51cmwpO1xuICAgIH1cblxuICAgIHZhciBkYXRhID0gZXZlbnQuZGF0YTtcbiAgICB2YXIgbW9kdWxlID0gdGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXNbZGF0YS5tb2RdO1xuICAgIGlmIChtb2R1bGUpIHtcbiAgICAgIHZhciBtZXRob2QgPSBtb2R1bGVbZGF0YS5mbl07XG4gICAgICBpZiAobWV0aG9kKSB7XG4gICAgICAgIHZhciBtZXRob2RBcmdzID0gZGF0YS5hcmdzO1xuICAgICAgICBzZW5kUmVzcG9uc2UuX2NvbnRleHQgPSB7XG4gICAgICAgICAgYWRkb25fa2V5OiByZWcuZXh0ZW5zaW9uLmFkZG9uX2tleSxcbiAgICAgICAgICBrZXk6IHJlZy5leHRlbnNpb24ua2V5XG4gICAgICAgIH07XG4gICAgICAgIG1ldGhvZEFyZ3MucHVzaChzZW5kUmVzcG9uc2UpO1xuICAgICAgICBtZXRob2QuYXBwbHkobW9kdWxlLCBtZXRob2RBcmdzKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG4gIGRlZmluZUFQSU1vZHVsZShtb2R1bGUsIG1vZHVsZU5hbWUpe1xuICAgIGlmKCFtb2R1bGVOYW1lKXtcbiAgICAgIHRoaXMuX3JlZ2lzdGVyZWRBUElNb2R1bGVzLl9nbG9iYWxzID0gbW9kdWxlO1xuICAgIH1cbiAgICB0aGlzLl9yZWdpc3RlcmVkQVBJTW9kdWxlc1ttb2R1bGVOYW1lXSA9IG1vZHVsZTtcbiAgICByZXR1cm4gdGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXM7XG4gIH1cblxuICBfZnVsbEtleSh0YXJnZXRTcGVjKXtcbiAgICB2YXIga2V5ID0gdGFyZ2V0U3BlYy5hZGRvbl9rZXkgfHwgJ2dsb2JhbCc7XG4gICAgaWYodGFyZ2V0U3BlYy5rZXkpe1xuICAgICAga2V5ID0gYCR7a2V5fUBAJHt0YXJnZXRTcGVjLmtleX1gO1xuICAgIH1cblxuICAgIHJldHVybiBrZXk7XG4gIH1cblxuICBxdWV1ZUV2ZW50KHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50LCBjYWxsYmFjaykge1xuICAgIHZhciBsb2FkZWRfZnJhbWUsXG4gICAgdGFyZ2V0cyA9IHRoaXMuX2ZpbmRSZWdpc3RyYXRpb25zKHRhcmdldFNwZWMpO1xuXG4gICAgbG9hZGVkX2ZyYW1lID0gdGFyZ2V0cy5zb21lKCh0YXJnZXQpID0+IHtcbiAgICAgIHJldHVybiB0YXJnZXQucmVnaXN0ZXJlZF9ldmVudHMgIT09IHVuZGVmaW5lZDtcbiAgICB9LCB0aGlzKTtcblxuICAgIGlmKGxvYWRlZF9mcmFtZSl7XG4gICAgICB0aGlzLmRpc3BhdGNoKHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50LCBjYWxsYmFjayk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuX3BlbmRpbmdFdmVudHNbdGhpcy5fZnVsbEtleSh0YXJnZXRTcGVjKV0gPSB7XG4gICAgICAgIHR5cGUsXG4gICAgICAgIHRhcmdldFNwZWMsXG4gICAgICAgIGV2ZW50LFxuICAgICAgICBjYWxsYmFjayxcbiAgICAgICAgdGltZTogbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgICAgIHVpZDogVXRpbHMucmFuZG9tU3RyaW5nKClcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgX2hhbmRsZUV2ZW50UXVlcnkobWVzc2FnZSwgZXh0ZW5zaW9uKSB7XG4gICAgbGV0IGV4ZWN1dGVkID0ge307XG4gICAgbGV0IG5vdyA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgIGxldCBrZXlzID0gT2JqZWN0LmtleXModGhpcy5fcGVuZGluZ0V2ZW50cyk7XG4gICAga2V5cy5mb3JFYWNoKChpbmRleCkgPT4ge1xuICAgICAgbGV0IGVsZW1lbnQgPSB0aGlzLl9wZW5kaW5nRXZlbnRzW2luZGV4XTtcbiAgICAgIGlmKCAobm93IC0gZWxlbWVudC50aW1lKSA8PSBWQUxJRF9FVkVOVF9USU1FX01TKSB7XG4gICAgICAgIGV4ZWN1dGVkW2luZGV4XSA9IGVsZW1lbnQ7XG4gICAgICAgIGVsZW1lbnQudGFyZ2V0U3BlYy5hZGRvbl9rZXkgPSBleHRlbnNpb24uZXh0ZW5zaW9uLmFkZG9uX2tleTtcbiAgICAgICAgZWxlbWVudC50YXJnZXRTcGVjLmtleSA9IGV4dGVuc2lvbi5leHRlbnNpb24ua2V5O1xuICAgICAgICB0aGlzLmRpc3BhdGNoKGVsZW1lbnQudHlwZSwgZWxlbWVudC50YXJnZXRTcGVjLCBlbGVtZW50LmV2ZW50LCBlbGVtZW50LmNhbGxiYWNrLCBtZXNzYWdlLnNvdXJjZSk7XG4gICAgICB9XG4gICAgICBkZWxldGUgdGhpcy5fcGVuZGluZ0V2ZW50c1tpbmRleF07XG4gICAgfSk7XG5cbiAgICB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9uc1tleHRlbnNpb24uZXh0ZW5zaW9uX2lkXS5yZWdpc3RlcmVkX2V2ZW50cyA9IG1lc3NhZ2UuZGF0YS5hcmdzO1xuXG4gICAgcmV0dXJuIGV4ZWN1dGVkO1xuICB9XG5cbiAgZGlzcGF0Y2godHlwZSwgdGFyZ2V0U3BlYywgZXZlbnQsIGNhbGxiYWNrLCBzb3VyY2UpIHtcbiAgICBmdW5jdGlvbiBzZW5kRXZlbnQocmVnLCBldm50KSB7XG4gICAgICBpZiAocmVnLnNvdXJjZSkge1xuICAgICAgICB2YXIgbWlkO1xuICAgICAgICBpZiAoY2FsbGJhY2spIHtcbiAgICAgICAgICBtaWQgPSBVdGlscy5yYW5kb21TdHJpbmcoKTtcbiAgICAgICAgICB0aGlzLl9wZW5kaW5nQ2FsbGJhY2tzW21pZF0gPSBjYWxsYmFjaztcbiAgICAgICAgfVxuXG4gICAgICAgIHJlZy5zb3VyY2UucG9zdE1lc3NhZ2Uoe1xuICAgICAgICAgIHR5cGU6ICdldnQnLFxuICAgICAgICAgIG1pZDogbWlkLFxuICAgICAgICAgIGV0eXA6IHR5cGUsXG4gICAgICAgICAgZXZudDogZXZudFxuICAgICAgICB9LCByZWcuZXh0ZW5zaW9uLnVybCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBcIkNhbm5vdCBzZW5kIHBvc3QgbWVzc2FnZSB3aXRob3V0IGEgc291cmNlXCI7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIHJlZ2lzdHJhdGlvbnMgPSB0aGlzLl9maW5kUmVnaXN0cmF0aW9ucyh0YXJnZXRTcGVjIHx8IHt9KTtcbiAgICByZWdpc3RyYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKHJlZykge1xuICAgICAgaWYoc291cmNlKXtcbiAgICAgICAgcmVnLnNvdXJjZSA9IHNvdXJjZTtcbiAgICAgIH1cbiAgICAgIFV0aWxzLl9iaW5kKHRoaXMsIHNlbmRFdmVudCkocmVnLCBldmVudCk7XG4gICAgfSwgdGhpcyk7XG4gIH1cblxuICBfZmluZFJlZ2lzdHJhdGlvbnModGFyZ2V0U3BlYykge1xuICAgIGlmKHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zLmxlbmd0aCA9PT0gMCl7XG4gICAgICBVdGlscy5lcnJvcignbm8gcmVnaXN0ZXJlZCBleHRlbnNpb25zJywgdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnMpO1xuICAgICAgcmV0dXJuIFtdO1xuICAgIH1cbiAgICB2YXIga2V5cyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKHRhcmdldFNwZWMpO1xuICAgIHZhciByZWdpc3RyYXRpb25zID0gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnMpLm1hcCgoa2V5KSA9PiB7XG4gICAgICByZXR1cm4gdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnNba2V5XTtcbiAgICB9KTtcblxuICAgIHJldHVybiByZWdpc3RyYXRpb25zLmZpbHRlcihmdW5jdGlvbiAocmVnKSB7XG4gICAgICByZXR1cm4ga2V5cy5ldmVyeShmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIHJldHVybiByZWcuZXh0ZW5zaW9uW2tleV0gPT09IHRhcmdldFNwZWNba2V5XTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcmVnaXN0ZXJFeHRlbnNpb24oZXh0ZW5zaW9uX2lkLCBkYXRhKSB7XG4gICAgLy8gZGVsZXRlIGR1cGxpY2F0ZSByZWdpc3RyYXRpb25zXG4gICAgaWYoZGF0YS5leHRlbnNpb24uYWRkb25fa2V5ICYmIGRhdGEuZXh0ZW5zaW9uLmtleSl7XG4gICAgICBsZXQgZXhpc3RpbmdWaWV3ID0gdGhpcy5fZmluZFJlZ2lzdHJhdGlvbnMoe1xuICAgICAgICBhZGRvbl9rZXk6IGRhdGEuZXh0ZW5zaW9uLmFkZG9uX2tleSxcbiAgICAgICAga2V5OiBkYXRhLmV4dGVuc2lvbi5rZXlcbiAgICAgIH0pO1xuICAgICAgaWYoZXhpc3RpbmdWaWV3Lmxlbmd0aCAhPT0gMCl7XG4gICAgICAgIGRlbGV0ZSB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9uc1tleGlzdGluZ1ZpZXdbMF0uZXh0ZW5zaW9uX2lkXTtcbiAgICAgIH1cbiAgICB9XG4gICAgZGF0YS5leHRlbnNpb25faWQgPSBleHRlbnNpb25faWQ7XG4gICAgdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnNbZXh0ZW5zaW9uX2lkXSA9IGRhdGE7XG4gIH1cblxuICBnZXRBcGlTcGVjKCkge1xuICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICBmdW5jdGlvbiBjcmVhdGVNb2R1bGUobW9kdWxlTmFtZSkge1xuICAgICAgdmFyIG1vZHVsZSA9IHRoYXQuX3JlZ2lzdGVyZWRBUElNb2R1bGVzW21vZHVsZU5hbWVdO1xuICAgICAgaWYoIW1vZHVsZSl7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcInVucmVnaXN0ZXJlZCBBUEkgbW9kdWxlOiBcIiArIG1vZHVsZU5hbWUpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKG1vZHVsZSkucmVkdWNlKChhY2N1bXVsYXRvciwgZnVuY3Rpb25OYW1lKSA9PiB7XG4gICAgICAgICAgaWYgKHR5cGVvZiBtb2R1bGVbZnVuY3Rpb25OYW1lXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICBhY2N1bXVsYXRvcltmdW5jdGlvbk5hbWVdID0ge307IC8vIGNvdWxkIGhvbGQgZnVuY3Rpb24gbWV0YWRhdGEsIGVtcHR5IGZvciBub3dcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGFjY3VtdWxhdG9yO1xuICAgICAgfSwge30pO1xuICAgIH1cblxuICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0aGlzLl9yZWdpc3RlcmVkQVBJTW9kdWxlcykucmVkdWNlKChhY2N1bXVsYXRvciwgbW9kdWxlTmFtZSkgPT4ge1xuICAgICAgICBhY2N1bXVsYXRvclttb2R1bGVOYW1lXSA9IGNyZWF0ZU1vZHVsZShtb2R1bGVOYW1lKTtcbiAgICAgICAgcmV0dXJuIGFjY3VtdWxhdG9yO1xuICAgIH0sIHt9KTtcbiAgfVxuXG5cbiAgLy8gdmFsaWRhdGUgb3JpZ2luIG9mIHBvc3RNZXNzYWdlXG4gIF9jaGVja09yaWdpbihldmVudCwgcmVnKSB7XG4gICAgbGV0IG5vX3NvdXJjZV90eXBlcyA9IFsnaW5pdCcsICdldmVudF9xdWVyeSddO1xuICAgIGxldCBpc1ZhbGlkT3JpZ2luID0gcmVnICYmIHJlZy5leHRlbnNpb24udXJsLmluZGV4T2YoZXZlbnQub3JpZ2luKSA9PT0gMCAmJlxuICAgICAgICAoKCFyZWcuc291cmNlICYmIG5vX3NvdXJjZV90eXBlcy5pbmRleE9mKGV2ZW50LmRhdGEudHlwZSkgPiAtMSkgfHwgZXZlbnQuc291cmNlID09PSByZWcuc291cmNlKTtcbiAgICBpZighaXNWYWxpZE9yaWdpbikge1xuICAgICAgICBVdGlscy53YXJuKFwiRmFpbGVkIHRvIHZhbGlkYXRlIG9yaWdpbjogXCIgKyBldmVudC5vcmlnaW4pO1xuICAgIH1cbiAgICByZXR1cm4gaXNWYWxpZE9yaWdpbjtcbiAgfVxuXG4gIGdldFJlZ2lzdGVyZWRFeHRlbnNpb25zKGZpbHRlcikge1xuICAgIGlmKGZpbHRlcikge1xuICAgICAgcmV0dXJuIHRoaXMuX2ZpbmRSZWdpc3RyYXRpb25zKGZpbHRlcik7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9ucztcbiAgfVxuXG4gIHVucmVnaXN0ZXJFeHRlbnNpb24oZmlsdGVyKSB7XG4gICAgbGV0IHJlZ2lzdHJhdGlvbnMgPSB0aGlzLl9maW5kUmVnaXN0cmF0aW9ucyhmaWx0ZXIpO1xuICAgIGlmKHJlZ2lzdHJhdGlvbnMubGVuZ3RoICE9PSAwKXtcbiAgICAgIHJlZ2lzdHJhdGlvbnMuZm9yRWFjaChmdW5jdGlvbihyZWdpc3RyYXRpb24pIHtcbiAgICAgICAgZGVsZXRlIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW3JlZ2lzdHJhdGlvbi5leHRlbnNpb25faWRdO1xuICAgICAgfSwgdGhpcyk7XG4gICAgfVxuICB9XG5cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBYRE1SUEM7Il19
