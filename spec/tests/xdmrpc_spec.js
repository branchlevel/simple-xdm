import XDMRPC from 'host/xdmrpc';

describe("XDMRPC", function () {

  it('checks for a valid origin', function () {
    let instance = new XDMRPC(),
        origin = "https://www.example.com",
        check = instance._checkOrigin({
          source: {},
          data: {
            type: 'init'
          },
          origin: origin
        }, {
          extension: {
            url: origin
          }
        });
      expect(check).toBe(true);
  });

  describe('event dispatch handling', function() {
    var instance,
      target_spec,
      extension_id;

    beforeEach(function(){
      instance = new XDMRPC();
      target_spec = {
        addon_key: 'some-key',
        key: 'module-key'
      };
      extension_id = `${target_spec.addon_key}__${target_spec.key}`;
      instance.registerExtension(extension_id, {
        extension: target_spec
      });
    });

    it('dispatches an event if there are active frames', function () {
      instance._registeredExtensions[extension_id].registered_events = {};

      spyOn(instance, 'dispatch');
      instance.queueEvent('some-event', target_spec, {});
      expect(instance.dispatch).toHaveBeenCalled();
    });

    it('queues the event if no frames are found', function () {
      var event_type = 'some-event',
        pending;
      spyOn(instance, 'dispatch');
      instance.queueEvent(event_type, target_spec, {});
      pending = instance._pendingEvents[instance._fullKey(target_spec)];
      expect(instance.dispatch).not.toHaveBeenCalled();
      expect(pending).toEqual(jasmine.any(Object));
      expect(pending.type).toEqual(event_type);
    });

  });






});