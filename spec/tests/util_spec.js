import Util from 'common/util';

describe("Util", function () {

  it("randomString should create a random string", function () {
    let firstString = Util.randomString(),
        secondString = Util.randomString();

    expect(firstString).not.toEqual(secondString);
  });

});