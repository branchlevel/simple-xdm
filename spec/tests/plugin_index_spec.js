
describe("Plugin", function () {
  let window_name = window.name,
  instance;

  beforeEach(function () {
    window.name = JSON.stringify({
      origin: window.location.origin,
      api: { 
        somemodule: { "method1": {} }
      } 
    });
    instance = require('plugin/index');
  });

  afterEach(function () {
    window.name = window_name;
  });

  it("_checkOrigin checks for a valid origin", function () {
    let e = {
      origin: window.location.origin,
      source: window.parent
    };

    expect(instance._checkOrigin(e)).toBe(true);
  });

  it("_checkOrigin errors on invalid origin", function () {
    let e = {
      origin: 'http://www.example.com',
      source: window.parent
    };

    expect(instance._checkOrigin(e)).toBe(false);
  });


  it("_checkOrigin errors on invalid host", function () {
    let e = {
      origin: window.location.origin,
      source: window
    };

    expect(instance._checkOrigin(e)).toBe(false);
  });


});