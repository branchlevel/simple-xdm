var fs = require('fs');
var path = require('path');
var DISABLE_REPORTERS = process.env["DISABLE_REPORTERS"] !== undefined;
var HC_WEB_REPORTER = process.env["HC_WEB_REPORTER"] || "nyan"; // still defaults to the kitty

function getCwd() {
  var cwd = process.cwd();

  var configSuffix = "/config";
  if (cwd.substr(-1 * configSuffix.length) === configSuffix) {
    cwd = path.join(cwd, "..", "..");
  }

  return cwd;
}

module.exports = function (config) {
  var cwd = getCwd();
  var webpackConf = {
    cache: true,
    module: {
      loaders: [
        {test: /(?:\/src\/.*?\.js|\/spec\/.*?\.js)$/, loader: "babel-loader?cacheDirectory"},
        {test: /(?:\/src\/.*?\.json|\/spec\/.*?\.json)$/, loader: "json-loader"}
      ]
    },
    resolve: {
      alias: {}
    }
  };

  function getDirs(srcpath) {
    return fs.readdirSync(srcpath).filter(function(file) {
      return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
  }

  var base = cwd + '/src';
  getDirs(base).forEach(function (root) {
    webpackConf.resolve.alias[root] = base + '/' + root;
  });

  config.set({
    logLevel: config.LOG_INFO,
    quiet: true,
    client: {
      captureConsole: false
    },
    basePath: '../..',
    plugins: [
      'karma-jasmine',
      'karma-webpack',
      'karma-chrome-launcher',
      'karma-phantomjs-launcher',
      'karma-firefox-launcher',
      'karma-nyan-reporter'
    ],
    preprocessors: {
      './src/**/*.js': [ 'webpack' ],
      './spec/tests/*.js': [ 'webpack' ]
    },
    webpack: webpackConf,
    webpackServer: {
      quiet: true
    },
    junitReporter: {
      outputFile: './test_out/results.xml',
      suite: 'unit'
    },
    reporters: DISABLE_REPORTERS ? [] : [HC_WEB_REPORTER, 'junit'],
    frameworks: ['jasmine'],
    browsers: ['Firefox', 'PhantomJS', 'Chrome'],
    files: [
      './node_modules/lodash/index.js',
      './spec/tests/*.js'
    ],
    browserNoActivityTimeout: 80000000,
    autoWatch: true,
    exclude: [
    ]
  });
};