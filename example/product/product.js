AP.defineModule('messages', {

    clear: function(id) {
        alert('clear: ' + id);
    },
    error: function(title, body, options) {
        alert('error: ' + title + ' -> ' + body);
    },
    generic: function(title, body, options) {
        alert('generic: ' + title + ' -> ' + body);
    },
    hint: function(title, body, options) {
        alert('hint: ' + title + ' -> ' + body);
    },
    info: function(title, body, options) {
        alert('info: ' + title + ' -> ' + body);
    },
    success: function(title, body, options) {
        alert('success: ' + title + ' -> ' + body);
    },
    warning: function(title, body, options) {
        alert('warning: ' + title + ' -> ' + body);
    },
});

AP.defineGlobals({
   request: function(options, cb) {
       setTimeout(function() {
           cb({statusCode: 200, response: options});
       }, 10);
   }
});