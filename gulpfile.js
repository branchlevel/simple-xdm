var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var derequire = require('gulp-derequire');
var envify = require('envify/custom');
var gulp = require('gulp');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var unreachableBranch = require('unreachable-branch-transform');
var watchify = require('watchify');
var runSequence = require('run-sequence');
var source = require('vinyl-source-stream');

function getTask(task) {
    return require('./gulp-tasks/' + task)(gulp);
}


function build(entryModule, distModule, options) {
    var bundler = browserify(entryModule, {
      debug: true,
      standalone: options.standalone || distModule
    }).transform(babelify)
      .transform(envify(options.env || {}))
      .transform(unreachableBranch);

    function rebundle() {
        var bundle = bundler.bundle()
            .on('error', function (err) {
                gutil.log(gutil.colors.red('Browserify error'), err.message);
                this.emit('end');
            })
            .pipe(source(distModule + '.js'))
            .pipe(buffer())
            .pipe(derequire());
            if(options.minify){
                bundle = bundle.pipe(uglify());
            }
        return bundle.pipe(gulp.dest('./dist'));
    }

    if (options.watch) {
        bundler = watchify(bundler);

        bundler.on('update', function () {
            gutil.log('Rebundling', gutil.colors.blue(entryModule));
            rebundle(bundler, options);
        });
    }

    gutil.log('Bundling', gutil.colors.blue(entryModule));
    return rebundle(bundler, options);
}

function buildPlugin(options) {
    options = options || {};
    return build('./src/plugin/index.js', (options.minify ? 'iframe.min' : 'iframe'), {
        standalone: 'AP',
        env: {ENV: 'plugin'},
        watch: options.watch,
        minify: options.minify
    });
}

function buildHost(options) {
    options = options || {};
    return build('./src/host/index.js', (options.minify ? 'host.min' : 'host'), {
        env: {ENV: 'host'},
        watch: options.watch,
        minify: options.minify
    });
}

gulp.task('lint', getTask('eslint'));
gulp.task('karma', getTask('karma'));

gulp.task('test', function(done) {
  runSequence('lint', 'karma', done);
});

gulp.task('test-ci', function(done) {
    runSequence('lint', 'karma-ci', done);
});

gulp.task('karma-ci', getTask('karma-ci'));

gulp.task('plugin:build', buildPlugin);
gulp.task('plugin:watch', buildPlugin.bind(null, {watch: true, minify: true}));
gulp.task('plugin:minify', buildPlugin.bind(null, {minify: true}));


gulp.task('host:build', buildHost);
gulp.task('host:watch', buildHost.bind(null, {watch: true, minify: true}));
gulp.task('host:minify', buildHost.bind(null, {minify: true}));

gulp.task('watch', ['plugin:watch', 'host:watch']);
gulp.task('build', ['host:build', 'plugin:build']);
gulp.task('minify', ['host:minify', 'plugin:minify']);

gulp.task('default', ['build', 'minify']);
