import XDMRPC from './xdmrpc';
import Utils from '../common/util';

class Connect {

  constructor() {
    this._xdm = new XDMRPC();
  }

  dispatch(type, targetSpec, event, callback) {
    this._xdm.queueEvent(type, targetSpec, event, callback);
    return this.getExtensions(targetSpec);
  }

  _createId(extension) {
    if(!extension.addon_key || !extension.key){
      throw Error('Extensions require addon_key and key');
    }
    return extension.addon_key + '__' + extension.key + '__' + Utils.randomString();
  }
  /**
  * Creates a new iframed module, without actually creating the DOM element.
  * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
  * the DOM element and returning the window reference.
  *
  * @param extension The extension definition. Example:
  *   {
  *     addonKey: 'my-addon',
  *     moduleKey: 'my-module',
  *     url: 'https://example.com/my-module'
  *   }
  *
  * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
  **/
  create(extension, initCallback) {
    let extension_id = this.registerExtension(extension, initCallback);

    let data = {
      extension_id: extension_id,
      api: this._xdm.getApiSpec(),
      origin: window.location.origin
    };

    return {
      id: extension_id,
      name: JSON.stringify(data),
      src: extension.url
    };
  }

  registerExtension(extension, initCallback) {
    let extension_id = this._createId(extension);
    this._xdm.registerExtension(extension_id, {
      extension: extension,
      initCallback: initCallback
    });
    return extension_id;
  }

  defineModule(moduleName, module) {
    this._xdm.defineAPIModule(module, moduleName);
  }

  defineGlobals(module) {
    this._xdm.defineAPIModule(module);
  }

  getExtensions(filter) {
    return this._xdm.getRegisteredExtensions(filter);
  }

  unregisterExtension(filter) {
    return this._xdm.unregisterExtension(filter);
  }

}

module.exports = new Connect();