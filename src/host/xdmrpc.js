/**
* Postmessage format:
*
* Initialization
* --------------
* {
*   type: 'init',
*   eid: 'my-addon__my-module-xyz'  // the extension identifier, unique across iframes
* }
*
* Request
* -------
* {
*   type: 'req',
*   eid: 'my-addon__my-module-xyz',  // the extension identifier, unique for iframe
*   mid: 'xyz',  // a unique message identifier, required for callbacks
*   mod: 'cookie',  // the module name
*   fn: 'read',  // the method name
*   args: [arguments]  // the method arguments
* }
*
* Response
* --------
* {
*   type: 'resp'
*   eid: 'my-addon__my-module-xyz',  // the extension identifier, unique for iframe
*   mid: 'xyz',  // a unique message identifier, obtained from the request
*   args: [arguments]  // the callback arguments
* }
*
* Event
* -----
* {
*   type: 'evt',
*   etyp: 'some-event',
*   evnt: { ... }  // the event data
*   mid: 'xyz', // a unique message identifier for the event
* }
**/

import Utils from '../common/util';
import PostMessage from '../common/postmessage';

let VALID_EVENT_TIME_MS = 30000; //30 seconds

class XDMRPC extends PostMessage {

  constructor(config) {
    config = config || {};
    super(config);
    this._registeredExtensions = config.extensions || {};
    this._registeredAPIModules = {};
    this._pendingCallbacks = {};
    this._pendingEvents = {};
    this._messageHandlers = {
      init: this._handleInit,
      req: this._handleRequest,
      resp: this._handleResponse,
      event_query: this._handleEventQuery
    };
  }

  _handleInit(event, reg) {
    this._registeredExtensions[reg.extension_id].source = event.source;
    if (reg.initCallback) {
      reg.initCallback(event.data.eid);
      delete reg.initCallback;
    }
  }

  _handleResponse(event) {
    var data = event.data;
    var pendingCallback = this._pendingCallbacks[data.mid];
    if (pendingCallback) {
      delete this._pendingCallbacks[data.mid];
      pendingCallback.apply(window, data.args);
    }
  }

  _handleRequest(event, reg) {
    function sendResponse() {
      var args = Utils.argumentsToArray(arguments);
      event.source.postMessage({
        mid: event.data.mid,
        type: 'resp',
        args: args
      }, reg.extension.url);
    }

    var data = event.data;
    var module = this._registeredAPIModules[data.mod];
    if (module) {
      var method = module[data.fn];
      if (method) {
        var methodArgs = data.args;
        sendResponse._context = {
          addon_key: reg.extension.addon_key,
          key: reg.extension.key
        };
        methodArgs.push(sendResponse);
        method.apply(module, methodArgs);
      }
    }
  }


  defineAPIModule(module, moduleName){
    if(!moduleName){
      this._registeredAPIModules._globals = module;
    }
    this._registeredAPIModules[moduleName] = module;
    return this._registeredAPIModules;
  }

  _fullKey(targetSpec){
    var key = targetSpec.addon_key || 'global';
    if(targetSpec.key){
      key = `${key}@@${targetSpec.key}`;
    }

    return key;
  }

  queueEvent(type, targetSpec, event, callback) {
    var loaded_frame,
    targets = this._findRegistrations(targetSpec);

    loaded_frame = targets.some((target) => {
      return target.registered_events !== undefined;
    }, this);

    if(loaded_frame){
      this.dispatch(type, targetSpec, event, callback);
    } else {
      this._pendingEvents[this._fullKey(targetSpec)] = {
        type,
        targetSpec,
        event,
        callback,
        time: new Date().getTime(),
        uid: Utils.randomString()
      };
    }
  }

  _handleEventQuery(message, extension) {
    let executed = {};
    let now = new Date().getTime();
    let keys = Object.keys(this._pendingEvents);
    keys.forEach((index) => {
      let element = this._pendingEvents[index];
      if( (now - element.time) <= VALID_EVENT_TIME_MS) {
        executed[index] = element;
        element.targetSpec.addon_key = extension.extension.addon_key;
        element.targetSpec.key = extension.extension.key;
        this.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
      }
      delete this._pendingEvents[index];
    });

    this._registeredExtensions[extension.extension_id].registered_events = message.data.args;

    return executed;
  }

  dispatch(type, targetSpec, event, callback, source) {
    function sendEvent(reg, evnt) {
      if (reg.source) {
        var mid;
        if (callback) {
          mid = Utils.randomString();
          this._pendingCallbacks[mid] = callback;
        }

        reg.source.postMessage({
          type: 'evt',
          mid: mid,
          etyp: type,
          evnt: evnt
        }, reg.extension.url);
      } else {
        throw "Cannot send post message without a source";
      }
    }

    var registrations = this._findRegistrations(targetSpec || {});
    registrations.forEach(function (reg) {
      if(source){
        reg.source = source;
      }
      Utils._bind(this, sendEvent)(reg, event);
    }, this);
  }

  _findRegistrations(targetSpec) {
    if(this._registeredExtensions.length === 0){
      Utils.error('no registered extensions', this._registeredExtensions);
      return [];
    }
    var keys = Object.getOwnPropertyNames(targetSpec);
    var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map((key) => {
      return this._registeredExtensions[key];
    });

    return registrations.filter(function (reg) {
      return keys.every(function (key) {
        return reg.extension[key] === targetSpec[key];
      });
    });
  }

  registerExtension(extension_id, data) {
    // delete duplicate registrations
    if(data.extension.addon_key && data.extension.key){
      let existingView = this._findRegistrations({
        addon_key: data.extension.addon_key,
        key: data.extension.key
      });
      if(existingView.length !== 0){
        delete this._registeredExtensions[existingView[0].extension_id];
      }
    }
    data.extension_id = extension_id;
    this._registeredExtensions[extension_id] = data;
  }

  getApiSpec() {
    let that = this;
    function createModule(moduleName) {
      var module = that._registeredAPIModules[moduleName];
      if(!module){
        throw new Error("unregistered API module: " + moduleName);
      }
      return Object.getOwnPropertyNames(module).reduce((accumulator, functionName) => {
          if (typeof module[functionName] === 'function') {
              accumulator[functionName] = {}; // could hold function metadata, empty for now
          }
          return accumulator;
      }, {});
    }

    return Object.getOwnPropertyNames(this._registeredAPIModules).reduce((accumulator, moduleName) => {
        accumulator[moduleName] = createModule(moduleName);
        return accumulator;
    }, {});
  }


  // validate origin of postMessage
  _checkOrigin(event, reg) {
    let no_source_types = ['init', 'event_query'];
    let isNoSourceType = reg && !reg.source && no_source_types.indexOf(event.data.type) > -1;
    let sourceTypeMatches = reg && event.source === reg.source;
    let hasExtensionUrl = reg && reg.extension.url.indexOf(event.origin) === 0;
    let isValidOrigin = hasExtensionUrl && (isNoSourceType || sourceTypeMatches);
    if(!isValidOrigin) {
        Utils.warn("Failed to validate origin: " + event.origin);
    }
    return isValidOrigin;
  }

  getRegisteredExtensions(filter) {
    if(filter) {
      return this._findRegistrations(filter);
    }
    return this._registeredExtensions;
  }

  unregisterExtension(filter) {
    let registrations = this._findRegistrations(filter);
    if(registrations.length !== 0){
      registrations.forEach(function(registration) {
        delete this._registeredExtensions[registration.extension_id];
      }, this);
    }
  }

}

module.exports = XDMRPC;