import Util from '../common/util';
import PostMessage from '../common/postmessage';

class AP extends PostMessage {

  constructor() {
    super();
    this._data = this._parseInitData();
    this._host = window.parent;
    this._hostModules = {};
    this._eventHandlers = {};
    this._pendingCallbacks = {};
    this._setupAPI(this._data.api);
    this._messageHandlers = {
        resp: this._handleResponse,
        evt: this._handleEvent
    };

    this._sendInit();
  }
  /**
  * The initialization data is passed in when the iframe is created as its 'name' attribute.
  * Example:
  * {
  *   extension_id: The ID of this iframe as defined by the host
  *   origin: 'https://example.org'  // The parent's window origin
  *   api: {
  *     _globals: { ... },
  *     messages = {
  *       clear: {},
  *       ...
  *     },
  *     ...
  *   }
  * }
  **/
  _parseInitData(data) {
    try {
      return JSON.parse(data || window.name);
    } catch (e) {
      return {};
    }
  }

  _createModule(moduleName, api) {
    return Object.getOwnPropertyNames(api).reduce((accumulator, functionName) => {
      accumulator[functionName] = this._createMethodHandler({
          mod: moduleName,
          fn: functionName
      });
      return accumulator;
    }, {});
  }

  _setupAPI(api) {
    this._hostModules = Object.getOwnPropertyNames(api).reduce((accumulator, moduleName) => {
        accumulator[moduleName] = this._createModule(moduleName, api[moduleName]);
        return accumulator;
    }, {});

    Object.getOwnPropertyNames(this._hostModules._globals || {}).forEach((global) => {
        this[global] = this._hostModules._globals[global];
    });
  }

  _pendingCallback(mid, fn){
    this._pendingCallbacks[mid] = fn;
  }

  _createMethodHandler(methodData) {
    let methodHandler = function () {
      let mid,
          args = Util.argumentsToArray(arguments);
      if (Util.hasCallback(args)) {
        mid = Util.randomString();
        this._pendingCallback(mid, args.pop());
      }
      this._host.postMessage({
          eid: this._data.extension_id,
          type: 'req',
          mid: mid,
          mod: methodData.mod,
          fn: methodData.fn,
          args: args
      }, this._data.origin);
    };

    return Util._bind(this, methodHandler);
  }

  _handleResponse(event) {
    var data = event.data;
    var pendingCallback = this._pendingCallbacks[data.mid];
    if (pendingCallback) {
      delete this._pendingCallbacks[data.mid];
      pendingCallback.apply(window, data.args);
    }
  }

  _handleEvent(event) {
    var sendResponse = function () {
      var args = Util.argumentsToArray(arguments);
      event.source.postMessage({
          eid: this._data.extension_id,
          mid: event.data.mid,
          type: 'resp',
          args: args
      }, this._data.origin);
    };
    sendResponse = Util._bind(this, sendResponse);
    var data = event.data;
    var handler = this._eventHandlers[data.etyp];
    if (handler) {
        handler(data.evnt, sendResponse);
    } else if (data.mid) {
        sendResponse();
    }
  }

  _checkOrigin(event) {
      return event.origin === this._data.origin && event.source === this._host;
  }

  _sendInit() {
    this._host.postMessage({
        eid: this._data.extension_id,
        type: 'init'
    }, this._data.origin);
  }

  require(modules, callback) {
    let requiredModules = Array.isArray(modules) ? modules : [modules],
        args = requiredModules.map((module) => {
          return this._hostModules[module];
        });
    callback.apply(window, args);
  }

  register(handlers) {
    this._eventHandlers = handlers || {};
    this._host.postMessage({
      eid: this._data.extension_id,
      type: 'event_query',
      args: Object.getOwnPropertyNames(handlers)
    }, this._data.origin);
  }

}

module.exports = new AP();
