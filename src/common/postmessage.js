import Util from './util';
class PostMessage {

  constructor(data) {
    let d = data || {};
    this._registerListener(d.listenOn);
  }

 // listen for postMessage events (defaults to window).
  _registerListener(listenOn) {
    if(!listenOn || !listenOn.addEventListener) {
      listenOn = window;
    }
    listenOn.addEventListener("message", Util._bind(this, this._receiveMessage), false);
  }

  _receiveMessage (event) {
    let extensionId = event.data.eid,
    reg;

    if(extensionId && this._registeredExtensions){
      reg = this._registeredExtensions[extensionId];
    }

    if (!this._checkOrigin(event, reg)) {
      return false;
    }

    var handler = this._messageHandlers[event.data.type];
    if (handler) {
      handler.call(this, event, reg);
    }
  }

}

module.exports = PostMessage;