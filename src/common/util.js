const LOG_PREFIX = "[Simple-XDM] ";

class Util {
  randomString() {
    return Math.floor(Math.random() * 1000000000).toString(16);
  }
  // might be un-needed
  argumentsToArray(arrayLike) {
    var array = [];
    for (var i = 0; i < arrayLike.length; i++) {
      array.push(arrayLike[i]);
    }
    return array;
  }

  hasCallback(args) {
    var length = args.length;
    return length > 0 && typeof args[length - 1] === 'function';
  }

  error(msg) {
      if (window.console) {
          console.error(LOG_PREFIX + msg);
      }
  }

  warn(msg) {
      if (window.console) {
          console.warn(LOG_PREFIX + msg);
      }
  }

  _bind(thisp, fn){
    if(Function.prototype.bind) {
      return fn.bind(thisp);
    }
    return function () {
      return fn.apply(thisp, arguments);
    };
  }



}

module.exports = new Util();